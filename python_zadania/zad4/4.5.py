#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


def odwracanieIter(L, left, right):
    for x in range(int((right - left + 1) / 2)):
        L[left + x], L[right - x] = L[right - x], L[left + x]


def odwracanieRek(L, left, right):
    if left < right:
        L[left], L[right] = L[right], L[left]
        odwracanieRek(L, left + 1, right - 1)


lista = [1, 2, 3, 4, 5, 6, 7, 8, 9]
odwracanieIter(lista, 0, 8)
print(lista)

odwracanieIter(lista, 1, 4)
print(lista)
print()

lista = [1, 2, 3, 4, 5, 6, 7, 8, 9]
odwracanieRek(lista, 0, 8)
print(lista)

odwracanieRek(lista, 1, 4)
print(lista)
