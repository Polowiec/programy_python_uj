#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


def factorial(n):
    if n < 0 :
        return -1

    silnia= 1
    while n > 1:
        silnia*= n
        n-= 1
    return silnia


print(factorial(0))
print(factorial(1))
print(factorial(2))
print(factorial(3))
print(factorial(4))
print(factorial(5))