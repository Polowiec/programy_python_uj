#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


def flatten(seq):
    lista = list()
    dodajElementyDoListy(lista, seq)
    return lista


def dodajElementyDoListy(L, sequence):
    if isinstance(sequence, (list, tuple)):
        for elem in sequence:
            dodajElementyDoListy(L, elem)
    else:
        L.append(sequence)


seq = [1, (2, 3), [], [4, (5, 6, 7)], 8, [9]]
print(flatten(seq))