#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

X = "qwerty"
def func1():
    print(X)
func1()
# "qwerty", nie istnieje zmienna lokalna, wiec odniesienie do globalnej


Y = "qwerty"
def func2():
    Y = "abc"
func2()
print(Y)
# "qwerty" w funkcji nastepuje zmiana wartosci dla zmiennej lokalnej, poza funkcja odniesieni do globalnej


Z = "qwerty"
def func3():
    global Z
    Z = "abc"
func3()
print(Z)
# "abc" , zadeklarowanie Z jako globalnego, przez co funkcja zmienia wartosc zmiennej