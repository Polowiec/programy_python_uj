#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


def stworzMiarke(dlugosc):
    if dlugosc <= 0:
        return ""

    miarka = "|"
    for x in range(dlugosc):
        miarka += "....|"
    miarka += "\n"

    miarka += "0"
    for x in range(dlugosc):
        miarka += str(x + 1).rjust(5)

    return miarka


def stworzKratke(szerokosc, wysokosc):
    if szerokosc <= 0 or wysokosc <= 0:
        return ""

    linia_pelna = "+"
    linia_pusta = "|"
    for x in range(szerokosc):
        linia_pelna += "+".rjust(4, '-')
        linia_pusta += "|".rjust(4)

    kratka = linia_pelna
    for x in range(wysokosc):
        kratka += "\n" + linia_pusta + "\n" + linia_pelna

    return kratka


print(stworzMiarke(15))
print()
print(stworzKratke(4, 2))
print()
print(stworzKratke(7, 1))
