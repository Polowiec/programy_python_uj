#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


def sum_seq(sequence):
    if isinstance(sequence, (list, tuple)):
        suma = 0
        for elem in sequence:
            suma += sum_seq(elem)
        return suma
    else:
        return sequence


seq = [1, (2, 3), [], [4, (5, 6, 7)], 8, [9]]
print(sum_seq(seq))

seq = [1, (2, 3), [[[[1], 1, [1]]], [3]], [4, (5, 6, 7)], 8, [9]]
print(sum_seq(seq))
