#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


def fibonacci(n):
    if n < 0:
        return -1

    if n == 0:
        return 0

    poprz = 0
    akt = 1
    for x in range(n-1):
        akt, poprz = akt + poprz, akt
    return akt

print(fibonacci(0))
print(fibonacci(1))
print(fibonacci(2))
print(fibonacci(3))
print(fibonacci(4))
print(fibonacci(5))
