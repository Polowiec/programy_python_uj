#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from cyclegroup import *

print("Grupa cykliczna N= 3")
generujGrupeCykliczna(3)
print("Grupa cykliczna N= 5")
generujGrupeCykliczna(5)
print()
print("Grupa cykliczna z generatora (0, 1, 2)(3, 4)(5)")
generujGrupeCyklicznaZPermutacji([1, 2, 0, 4, 3, 5])
print("Grupa cykliczna z generatorow (0, 1, 2) i (3, 4)")
generujGrupeZPermutacji([1, 2, 0, 3, 4, 5], [0, 1, 2, 4, 3, 5])
