#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

N = 10


def setN(number):
    if number < 1:
        raise ValueError
    N = number
    jeden = range(N)


def mul_perm(perm1, perm2):  # perm1*perm2
    rozm = len(perm1)
    if rozm != len(perm2):
        raise ValueError
    wynik = [0] * rozm
    for x in range(rozm):
        wynik[x] = perm1[perm2[x]]
    return wynik


def invert_perm(perm):  # permutacja odwrotna
    rozm = len(perm)
    wynik = [0] * rozm
    for x in range(rozm):
        wynik[perm[x]] = x
    return wynik


def is_identity(perm):  # bool, czy identyczność
    rozm = len(perm)
    for x in range(rozm):
        if perm[x] != x:
            return False
    return True


def find_order(perm):  # rząd (krotność) permutacji
    rozm = len(perm)
    j = [x for x in range(rozm)]
    rzad = 1

    p = list(perm)
    while p != j:
        p = mul_perm(p, perm)
        rzad += 1
    return rzad


jeden = range(N)  # identyczność

import unittest


class TestFrac(unittest.TestCase):
    def setUp(self):
        self.jedn = [0, 1, 2, 3]

    def test_mul(self):
        self.assertEqual(mul_perm([3, 1, 2, 0], self.jedn), [3, 1, 2, 0])
        self.assertEqual(mul_perm(self.jedn, [3, 1, 2, 0]), [3, 1, 2, 0])
        self.assertEqual(mul_perm([3, 1, 0, 2], [3, 1, 2, 0]), [2, 1, 0, 3])

    def test_invert(self):
        self.assertEqual(invert_perm(self.jedn), self.jedn)
        self.assertEqual(invert_perm([3, 0, 1, 2]), [1, 2, 3, 0])
        p1 = [3, 1, 5, 4, 0, 2]
        self.assertEqual(mul_perm(p1, invert_perm(p1)), [0, 1, 2, 3, 4, 5])

    def test_identity(self):
        self.assertTrue(is_identity([0, 1, 2]))
        self.assertTrue(is_identity([0, 1, 2, 3, 4, 5, 6]))
        self.assertTrue(is_identity([0]))
        self.assertFalse(is_identity([1, 0]))
        self.assertFalse(is_identity([0, 1, 3, 2]))

    def test_order(self):
        self.assertEqual(find_order(self.jedn), 1)
        self.assertEqual(find_order([1, 0, 2, 3, 4]), 2)
        self.assertEqual(find_order([1, 0, 2, 4, 3]), 2)
        self.assertEqual(find_order([1, 0, 4, 2, 3]), 6)
        self.assertEqual(find_order([4, 0, 1, 2, 3]), 5)

    def tearDown(self): pass


if __name__ == '__main__':
    unittest.main()  # uruchamia wszystkie testy
