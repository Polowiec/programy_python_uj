#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from symetricgroup import *

print("Grupa S_2:")
generujGrupe(2)
print()
print("Grupa S_3:")
generujGrupe(3)
print()
print("Grupa S_4:")
generujGrupe(4)
print()
print("Grupa S_5:")
generujGrupe(5)