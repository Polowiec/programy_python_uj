#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from perm import *
from cyclegroup import rozlozNaCykle


def generujGeneratory(rozmiar):
    if rozmiar <= 1:
        raise ValueError
    
    generatory = []
    permJedn = [x for x in range(rozmiar)]

    for x in range(rozmiar - 1):
        gen = list(permJedn)
        gen[x], gen[x + 1] = gen[x + 1], gen[x]
        generatory.append(gen)
    return generatory


def generujGrupe(rozmiar):
    if rozmiar <= 1:
        raise ValueError

    start = generujGeneratory(rozmiar)
    nowe = list(start)
    while nowe != []:
        elem = nowe[-1]
        del nowe[-1]
        for e in start:
            mul = mul_perm(elem, e)
            if not mul in start:
                start.append(mul)
                nowe.append(mul)
    l = 1
    for x in start:
        perm = str(x)
        cykle = rozlozNaCykle(x)
        c = ""
        for x in cykle:
            c += str(tuple(x))
        print("Permutacja " + str(l) + ": " + perm + "  ; cykle: " + c)
        l += 1
