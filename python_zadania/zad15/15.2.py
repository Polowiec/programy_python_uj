#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from sudokuperm import *


def perm4x4():
    print("Grupa permutacji sudoku 4x4:")
    s = generateSudoku(4)
    for x in range(4):
        print("Perm %s" % (x * 4))
        rysuj(s, 4, 2)
        w1 = permWierszy4x4(s)
        w2 = permKolumn4x4(s)
        w3 = permKolumn4x4(w1)
        print("Perm %s" % (x * 4 + 1))
        rysuj(w1, 4, 2)
        print("Perm %s" % (x * 4 + 2))
        rysuj(w2, 4, 2)
        print("Perm %s" % (x * 4 + 3))
        rysuj(w3, 4, 2)
        s = obrot(s)


def perm9x9():
    print("Grupa permutacji sudoku 9x9:")
    s = generateSudoku(9)
    kombinacje = []
    kombinacje2 = []
    # permutacja wierszy
    for x in range(3):
        w = permWierszy9x9(s, 0, x)
        kombinacje.append(w)
        w2 = permWierszy9x9(w, 1, 2)
        kombinacje.append(w2)
    # wszystkim powyzszym dokladam permutacje kolumn
    for elem in kombinacje:
        for x in range(3):
            w = permKolumn9x9(elem, 0, x)
            kombinacje2.append(w)
            w2 = permKolumn9x9(w, 1, 2)
            kombinacje2.append(w2)
    # obroty wszystkich kombinacji
    il = 0
    for elem in kombinacje2:
        for x in range(4):
            print("Perm %s" % il)
            il += 1
            rysuj(elem, 9, 3)
            elem = obrot(elem)


perm4x4()
perm9x9()
