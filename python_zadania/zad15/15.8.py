#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


# tabelka mnozenia kwaterionow
# +--+--+--+--+--+--+--+--+
# | 1| i| j| k|-1|-i|-j|-k|
# | i|-1| k|-j|-i| 1|-k| j|
# | j|-k|-1| i|-j| k| 1|-i|
# | k| j|-i|-1|-k|-j| i| 1|
# |-1|-i|-j|-k| 1| i| j| k|
# |-i| 1|-k| j| i|-1| k|-j|
# |-j| k| 1|-i| j|-k|-1| i|
# |-k|-j| i| 1| k| j|-i|-1|
# +--+--+--+--+--+--+--+--+

# tabelka zamieniona na liczby od 0-7 odpowiadajace permutacjom
# +--+--+--+--+--+--+--+--+
# | 0| 1| 2| 3| 4| 5| 6| 7|
# | 1| 4| 3| 6| 5| 0| 7| 2|
# | 2| 7| 4| 1| 6| 3| 0| 5|
# | 3| 2| 5| 4| 7| 6| 1| 0|
# | 4| 5| 6| 7| 0| 1| 2| 3|
# | 5| 0| 7| 2| 1| 4| 3| 6|
# | 6| 3| 0| 5| 2| 7| 4| 1|
# | 7| 6| 1| 0| 3| 2| 5| 4|
# +--+--+--+--+--+--+--+--+


from cyclegroup import *

perm1 = [1, 4, 3, 6, 5, 0, 7, 2]  # perm dla i
perm2 = [2, 7, 4, 1, 6, 3, 0, 5]  # perm dla j

generujGrupeZPermutacji(perm1, perm2)
