#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from perm import *


def generujCyklRzeduN(N):
    if N <= 0:
        raise ValueError
    cykl = [x + 1 for x in range(N - 1)]
    cykl.append(0)
    return cykl


def rozlozNaCykle(permutacja):
    rozmiar = len(permutacja)
    odwiedzone = [False] * rozmiar

    cykle = []
    for i in range(rozmiar):
        if odwiedzone[i]:
            continue
        cykl = []
        e = i
        while odwiedzone[e] == False:
            odwiedzone[e] = True
            cykl.append(e)
            e = permutacja[e]
        if len(cykl) > 1:
            cykle.append(cykl)
    return cykle


def generujGrupeCykliczna(rozmiar):
    if rozmiar < 1:
        raise ValueError

    cyklN = generujCyklRzeduN(rozmiar)
    kolejny = list(cyklN)
    l = 1
    while True:
        perm = str(kolejny)
        cykle = rozlozNaCykle(kolejny)
        c = ""
        for x in cykle:
            c += str(tuple(x))
        print("Permutacja " + str(l) + ": " + perm + "  ; cykle: " + c)

        l += 1
        kolejny = mul_perm(kolejny, cyklN)
        if kolejny == cyklN:
            break


def generujGrupeCyklicznaZPermutacji(cyklN):
    kolejny = list(cyklN)
    l = 1
    while True:
        perm = str(kolejny)
        cykle = rozlozNaCykle(kolejny)
        c = ""
        for x in cykle:
            c += str(tuple(x))
        print("Permutacja " + str(l) + ": " + perm + "  ; cykle: " + c)

        l += 1
        kolejny = mul_perm(kolejny, cyklN)
        if kolejny == cyklN:
            break


def generujGrupeZPermutacji(cykl1, cykl2):
    start = [cykl1, cykl2]
    nowe = [cykl1, cykl2]

    while nowe != []:
        elem = nowe[-1]
        del nowe[-1]
        for e in start:
            mul = mul_perm(elem, e)
            if not mul in start:
                start.append(mul)
                nowe.append(mul)
    l = 1
    for x in start:
        perm = str(x)
        cykle = rozlozNaCykle(x)
        c = ""
        for x in cykle:
            c += str(tuple(x))
        print("Permutacja " + str(l) + ": " + perm + "  ; cykle: " + c)
        l += 1
