#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from cyclegroup import *

# D_3
print("Grupa D_3 z generatorow (0, 1, 2) i (1, 2)")
generujGrupeZPermutacji([1, 2, 0], [0, 2, 1])
# S_3 = {(), (0,1), (0,2), (1,2), (0,1,2), (0,2,1)}
# generator zwrocil te same permutacje jak wyzej dla grupy S_3

# D_4
print()
print("Grupa D_4 z generatorow (0, 1, 2, 3) i (1, 3)")
generujGrupeZPermutacji([1, 2, 3, 0], [0, 3, 2, 1])

# D_5
print()
print("Grupa D_5 z generatorow (0, 1, 2, 3, 4) i (1, 4)(2, 3")
generujGrupeZPermutacji([1, 2, 3, 4, 0], [0, 4, 3, 2, 1])

# D_6
print()
print("Grupa D_6 z generatorow (0, 1, 2, 3, 4, 5) i (1, 5)(2, 4)")
generujGrupeZPermutacji([1, 2, 3, 4, 5, 0], [0, 5, 4, 3, 2, 1])

# D_2(rzędu 4)
print()
print("Grupa D_2 z generatorow (0, 1)(2, 3) i (0, 2)(1, 3)")
generujGrupeZPermutacji([1, 0, 3, 2], [2, 3, 0, 1])
# wyniki pokrywaja sie z tabelka grupowa D_2
# +----+---------+
# | E  | 0 1 2 3 |
# |C_2x| 1 0 3 2 |
# |C_2y| 2 3 0 1 |
# |C_2z| 3 2 1 0 |
# +----+---------+
