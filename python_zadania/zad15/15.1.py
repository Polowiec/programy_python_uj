#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from perm import *

print(mul_perm([2, 0, 3, 1], [1, 3, 2, 0]))
print(invert_perm([3, 1, 0, 2]))
print(is_identity([0, 1, 3, 2]))
print(find_order([3, 4, 2, 1, 0]))
