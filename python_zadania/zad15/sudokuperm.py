#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from math import sqrt


def generateSudoku(size):
    if size < 0:
        raise ValueError
    return [x for x in range(size * size)]


def rysuj(plansza, N, bok):
    poziomaBelka = "+"
    b = ""
    for x in range(2 * bok + bok + 1):
        b += "-"
    b += "+"
    for x in range(bok):
        poziomaBelka += b

    for x in range(N):
        if x % bok == 0:
            print(poziomaBelka)
        wiersz = ""
        for y in range(N):
            if y % bok == 0:
                wiersz += "| "
            wiersz += str(plansza[x * N + y]).rjust(2) + " "
        wiersz += "|"
        print(wiersz)
    print(poziomaBelka)


def permWierszy4x4(plansza):
    tab = list(plansza)
    for i in range(4):
        tab[0 * 4 + i], tab[2 * 4 + i] = tab[2 * 4 + i], tab[0 * 4 + i]
        tab[1 * 4 + i], tab[3 * 4 + i] = tab[3 * 4 + i], tab[1 * 4 + i]
    return tab


def permKolumn4x4(plansza):
    tab = list(plansza)
    for i in range(4):
        tab[i * 4 + 0], tab[i * 4 + 2] = tab[i * 4 + 2], tab[i * 4 + 0]
        tab[i * 4 + 1], tab[i * 4 + 3] = tab[i * 4 + 3], tab[i * 4 + 1]
    return tab


def obrot(plansza):
    rozmiar = sqrt(len(plansza))
    if rozmiar != int(rozmiar):
        raise ValueError
    rozmiar = int(rozmiar)

    tab = [0 for x in range(rozmiar * rozmiar)]
    for y in range(rozmiar):
        for x in range(rozmiar):
            tab[y * rozmiar + x] = plansza[(rozmiar - x - 1) * rozmiar + y]
    return tab


def permWierszy9x9(plansza, w1, w2):
    if w1 < 0 or w2 < 0 or w1 >= 3 or w2 >= 3:
        raise ValueError
    tab = list(plansza)
    for i in range(9):
        tab[w1 * 3 * 9 + i], tab[w2 * 3 * 9 + i] = tab[w2 * 3 * 9 + i], tab[w1 * 3 * 9 + i]
        tab[(w1 * 3 + 1) * 9 + i], tab[(w2 * 3 + 1) * 9 + i] = tab[(w2 * 3 + 1) * 9 + i], tab[(w1 * 3 + 1) * 9 + i]
        tab[(w1 * 3 + 2) * 9 + i], tab[(w2 * 3 + 2) * 9 + i] = tab[(w2 * 3 + 2) * 9 + i], tab[(w1 * 3 + 2) * 9 + i]
    return tab


def permKolumn9x9(plansza, w1, w2):
    if w1 < 0 or w2 < 0 or w1 >= 3 or w2 >= 3:
        raise ValueError
    tab = list(plansza)
    for i in range(9):
        tab[i * 9 + w1 * 3], tab[i * 9 + w2 * 3] = tab[i * 9 + w2 * 3], tab[i * 9 + w1 * 3]
        tab[i * 9 + w1 * 3 + 1], tab[i * 9 + w2 * 3 + 1] = tab[i * 9 + w2 * 3 + 1], tab[i * 9 + w1 * 3 + 1]
        tab[i * 9 + w1 * 3 + 2], tab[i * 9 + w2 * 3 + 2] = tab[i * 9 + w2 * 3 + 2], tab[i * 9 + w1 * 3 + 2]
    return tab
