#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

import random


def calc_pi(n=100):
    """Obliczanie liczby pi metodą Monte Carlo.
    n oznacza liczbę losowanych punktów."""
    if n <= 0:
        raise ValueError

    random.seed()
    trafione = 0
    # losowanie dla kwadratu z cwiartką okregu
    for x in range(n):
        l1, l2 = random.random(), random.random()
        if l1 ** 2 + l2 ** 2 <= 1:
            trafione += 1

    return 4.0 * trafione / n


print(calc_pi(10))
print(calc_pi(100))
print(calc_pi(1000))
print(calc_pi(10000))
print(calc_pi(100000))
print(calc_pi(1000000))
