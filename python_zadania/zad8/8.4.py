#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from math import sqrt

def heron(a, b, c):
    """Obliczanie pola powierzchni trójkąta za pomocą wzoru
    Herona. Długości boków trójkąta wynoszą a, b, c."""

    if a<= 0 or b <= 0 or c<= 0:
        raise ValueError
    if a + b <= c or a + c <= b or c + b <= a:
        raise ValueError

    p = 0.5 * (a + b + c)
    return sqrt(p * (p - a) * (p - b) * (p - c))


print(heron(3, 4, 5))
print(heron(5, 5, 5))