#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from math import sqrt


def solve2(a, b, c):
    """Rozwiązywanie równania kwadratowego a * x * x + b * x + c = 0."""
    if a == 0:
        if b != 0:
            print("Funkcja przedstawia rownanie liniowe, rozwiazaniem jest x= ", (-1.0*c/b));
        else:
            if c == 0:
                print("Równanie tożsamościowe")
            else:
                print("Rówanie sprzeczne")
    else:
        delta = b ** 2 - 4 * a * c
        if delta > 0:
            x1 = (-b - sqrt(delta)) / (2.0 * a)
            x2 = (-b + sqrt(delta)) / (2.0 * a)
            print("Funkcja posiada dwa rzeczywiste pierwiastki: x1= %s, x2= %s" % (x1, x2))
        elif delta == 0:
            x0 = -b / (2.0 * a)
            print("Funkcja posiada podwojny pierwiastek: x0= %s" % x0)
        else:
            r = -b / (2.0 * a)
            im = sqrt(-delta) / (2.0 * a)
            print("Fukcja posiada dwa pierwiastki zespolone: z1= %s -%si, z2= %s +%si" % (r, im, r, im))


solve2(1, 5, 6)
solve2(1, 4, 4)
solve2(1, 0, 2)
solve2(0, 0, 2)
solve2(0, 5, 2)
