#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


def ackermann(n, p):
    """Funkcja Ackermanna, n i p to liczby naturalne.
    Zachodzi A(0, p) = 1, A(n, 1) = 2n, A(n, 2) = 2**n,
    A(n, 3) = 2**A(n-1, 3).
    """
    if n == 0:
        return 1
    if p == 0 and n >= 0:
        if n == 1:
            return 2
        else:
            return n + 2
    if p >= 0 and n >= 1:
        return ackermann(ackermann(n - 1, p), p - 1)


for n in range(4):
    for p in range(4):
        print("ack(%s, %s)= %s" % (n, p, ackermann(n, p)))



##########  tabela   ###############
# n/p 0   1   2   3
# __|_______________
# 0 | 1   1   1   1
# 1 | 2   2   2   2
# 2 | 4   4   4   4
# 3 | 5   6   8   16

# dla wiekszego n lub p nastepuje przepelnienie maxymalnej glebokosci rekurencji
