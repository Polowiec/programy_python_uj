#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

wartP = dict()
wywolanP = 0
wywolanRek = 0


def funkcjaP(i, j):
    global wywolanP
    wywolanP += 1
    if (i, j) in wartP:
        return wartP[(i, j)]

    if i == 0 and j == 0:
        wartP[(0, 0)] = 0.5
        return 0.5
    elif j == 0:
        wartP[(i, 0)] = 0
        return 0
    elif i == 0:
        wartP[(0, j)] = 1
        return 1
    else:
        wynik = 0.5 * (funkcjaP(i - 1, j) + funkcjaP(i, j - 1))
        wartP[(i, j)] = wynik
        return wynik


def rekP(i, j):
    global wywolanRek
    wywolanRek += 1
    if i == 0 and j == 0:
        return 0.5
    elif j == 0:
        return 0
    elif i == 0:
        return 1
    else:
        return 0.5 * (funkcjaP(i - 1, j) + funkcjaP(i, j - 1))


print(funkcjaP(0, 0))
print(funkcjaP(4, 8))
print(funkcjaP(3, 5))
print(funkcjaP(10, 10))
print(rekP(0, 0))
print(rekP(4, 8))
print(rekP(3, 5))
print(rekP(10, 10))

print("fP - wywolan: " + str(wywolanP))
print("rekP - wywolan: " + str(wywolanRek))
print()

for x in range(10):
    linia = ""
    for y in range(10):
        linia += str(wartP[(x, y)]).rjust(20)
    print(linia)
