#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


def solve1(a, b, c):
    """Rozwiązywanie równania liniowego a x + b y + c = 0."""
    if a == 0 and b == 0:
        if c == 0:
            print("Równanie tożsamosciowe, spelnia je dowolna para (x, y)")
        else:
            print("Równanie sprzeczne, żadna para (x, y) nie spełnia równania")
    else:
        if a == 0:
            y = -c * 1.0 / b
            print("Równanie spełniaja wszystkie pary (x, y): y= %s, x dowolne" % y)
        elif b == 0:
            x = -c * 1.0 / a
            print("Równanie spełniaja wszystkie pary (x, y): x= %s, y dowolne" % x)
        else:
            w1 = -a * 1.0 / b
            w2 = -c * 1.0 / b
            if w2 > 0:
                print("Równanie spełniaja wszystkie pary (x, y): takie że y= %sx +%s" % (w1, w2))
            elif w2 == 0:
                print("Równanie spełniaja wszystkie pary (x, y): takie że y= %sx" % w1)
            else:
                print("Równanie spełniaja wszystkie pary (x, y): takie że y= %sx %s" % (w1, w2))


solve1(0, 0, 0)
solve1(0, 0, 3)
solve1(4, 0, 1)
solve1(0, 4, 1)
solve1(2, 4, 10)
solve1(2, 5, 0)
solve1(3, 5, -10)
