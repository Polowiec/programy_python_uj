#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from graph import *


# dla rzędu drabiny R wygenerowany graf będzie posiadał:
# 2*R wierzchołków,  3*R -2 krawędzi ( R > 0 )
def generujGrafDrabina(rzadDrabiny):
    if rzadDrabiny < 0:
        raise ValueError

    if rzadDrabiny == 0:
        return {}

    graf = {}
    # poczatek drabiny
    add_node(graf, 0)
    add_node(graf, 1)
    add_edge_undirected(graf, (0, 1))

    for x in range(rzadDrabiny - 1):
        elem = (x + 1) * 2
        add_node(graf, elem)
        add_node(graf, elem + 1)
        add_edge_undirected(graf, (elem, elem + 1))
        add_edge_undirected(graf, (elem, elem - 2))
        add_edge_undirected(graf, (elem + 1, elem - 1))
    return graf


# dla rzędu kwadratu R wygenerowany graf będzie posiadał:
# (R+1)**2 wierzchołków, 2R(R+1)
def generujGrafKwadrat(rzadKwadratu):
    if rzadKwadratu < 0:
        raise ValueError

    if rzadKwadratu == 0:
        return {}
    graf = {}
    add_node(graf, 0)
    for x in range(rzadKwadratu):
        add_node(graf, x + 1)
        add_edge_undirected(graf, (x, x + 1))

    for w in range(rzadKwadratu):
        add_node(graf, (w + 1) * (rzadKwadratu + 1))
        add_edge_undirected(graf, (w * (rzadKwadratu + 1), (w + 1) * (rzadKwadratu + 1)))
        for x in range(rzadKwadratu):
            add_node(graf, (w + 1) * (rzadKwadratu + 1) + x + 1)
            add_edge_undirected(graf, (w * (rzadKwadratu + 1) + x + 1, (w + 1) * (rzadKwadratu + 1) + x + 1))
            add_edge_undirected(graf, ((w + 1) * (rzadKwadratu + 1) + x, (w + 1) * (rzadKwadratu + 1) + x + 1))
    return graf
