#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from graph import *

graf = {}
add_node(graf, "A")
add_node(graf, "B")
add_node(graf, "C")
add_node(graf, "D")
add_node(graf, "E")
add_edge_directed(graf, ("A", "B"))
add_edge_directed(graf, ("A", "C"))
add_edge_directed(graf, ("E", "B"))
add_edge_directed(graf, ("E", "F"))
add_edge_directed(graf, ("F", "D"))


print(topsort(graf))