#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

import queue


def add_node(graph, node):
    """Wstawia wierzchołek do grafu."""
    if node not in graph:
        graph[node] = []


def add_edge_directed(graph, edge):
    """Dodaje krawędź do grafu skierowanego."""
    source, target = edge
    add_node(graph, source)
    add_node(graph, target)
    # Możemy wykluczyć pętle.
    if source == target:
        raise ValueError("pętle są zabronione")
    if target not in graph[source]:
        graph[source].append(target)


def add_edge_undirected(graph, edge):
    """Dodaje krawędź do grafu nieskierowanego."""
    source, target = edge
    add_node(graph, source)
    add_node(graph, target)
    # Możemy wykluczyć pętle.
    if source == target:
        raise ValueError("pętle są zabronione")
    if target not in graph[source]:
        graph[source].append(target)
    if source not in graph[target]:
        graph[target].append(source)


def list_nodes(graph):
    """Zwraca listę wierzchołków grafu."""
    return list(graph.keys())


def count_nodes(graph):
    """Zwraca ilosc wierzchołków grafu."""
    return len(graph)


def list_edges(graph):
    """Zwraca listę krawędzi (krotek) grafu."""
    L = []
    for source in graph:
        for target in graph[source]:
            L.append((source, target))
    return L


def count_edges(graph):
    """Zwraca liczbę krawędzi."""
    return len(list_edges(graph))


def print_graph(graph):
    """Wypisuje postać grafu na ekranie."""
    for source in graph:
        wiersz = ""
        for target in graph[source]:
            wiersz += str(target) + " "
        print(source, ": ", wiersz)


def save_edges(graph, filename):
    f = open(filename, 'w')
    for (x, y) in list_edges(graph):
        f.write(str(x) + " " + str(y) + "\n")
    f.close()


def create_heap(size):
    if size < 0:
        raise ValueError
    if size == 0:
        return {}
    graf = {}
    add_node(graf, 0)
    for x in range(size - 1):
        elem = x + 1
        rodzic = int((elem - 1) / 2)
        add_node(graf, elem)
        add_edge_undirected(graf, (elem, rodzic))
    return graf


def full_graph(size):
    graf = {}
    for x in range(size):
        add_node(graf, x)
    for x in range(size):
        for y in range(size):
            if x != y:
                add_edge_directed(graf, (x, y))
    return graf


def node_steps(graph):
    slownik = {}
    for source in graph:
        slownik[source] = len(graph[source])
    return slownik


def topsort(graph):
    # Słownik z liczbą poprzedników wierzchołka
    # (stopień wejściowy wierzchołka).
    in_edges = dict((node, 0) for node in graph)

    # Ręcznie zrobiona pętla po krawędziach.
    for source in graph:
        for target in graph[source]:
            in_edges[target] = in_edges[target] + 1

    # Zakończyło się wczytywanie danych.

    # Tworzymy listę wierzchołków posortowanych.
    vsort = []
    # Tworzymy kolejkę wierzchołków nie mających poprzedników.
    import queue
    q = queue.Queue()

    # Do kolejki idą pierwsze wierzchołki bez poprzedników.
    for node in graph:
        if in_edges[node] == 0:
            q.put(node)

    while not q.empty():
        source = q.get()  # weź wierzchołek z kolejki
        vsort.append(source)  # wstaw wierzchołek do rozwiązania
        # Usuwamy wszystkie krawędzie wychodzące z wierzchołka.
        for target in graph[source]:
            # Usuwamy poprzednika węzła target.
            in_edges[target] = in_edges[target] - 1
            # Jeżeli zero, to wstawiamy do kolejki.
            if in_edges[target] == 0:
                q.put(target)
    return vsort
