#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from graph import *
from siecigrafowe import *

drabina = generujGrafDrabina(3)
print("Drabina:")
print_graph(drabina)
# 0  --  1
# |      |
# 2  --  3
# |      |
# 4  --  5

print("\nKwadrat:")
kwadrat = generujGrafKwadrat(2)
print_graph(kwadrat)
# 0 -- 1 -- 2
# |    |    |
# 3 -- 4 -- 5
# |    |    |
# 6 -- 7 -- 8
