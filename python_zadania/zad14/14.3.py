#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from graph import *

graf = {}
add_node(graf, "A")
add_node(graf, "B")
add_node(graf, "C")
add_edge_directed(graf, ("A", "B"))
add_edge_directed(graf, ("A", "C"))
add_edge_directed(graf, ("C", "B"))
add_edge_directed(graf, ("B", "A"))

save_edges(graf, "krawedzie.txt")
