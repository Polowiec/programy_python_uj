#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from graph import *

graf = {}
add_node(graf, "A")
add_node(graf, "B")
add_node(graf, "C")
add_edge_directed(graf, ("A", "B"))
add_edge_directed(graf, ("A", "C"))
add_edge_directed(graf, ("C", "B"))
add_edge_directed(graf, ("B", "A"))

print(list_nodes(graf))
print(count_nodes(graf))
print(list_edges(graf))
print(count_edges(graf))

print()
print_graph(graf)
