#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from graph import *
from dijkstra import *

# Kod testujący moduł dijkstra.

import unittest


class TestDijkstra(unittest.TestCase):
    def setUp(self):
        pass

    def test_error1(self):
        g = Graph(True)
        self.assertRaises(ValueError, lambda: dijkstra(g, 0))
        g2 = Graph(True)
        g2.add_node(1)
        self.assertRaises(ValueError, lambda: dijkstra(g2, 0))

    def test_error2(self):
        g = Graph(True)
        g.add_node(1)
        g.add_node(2)
        g.add_edge(Edge(1, 2, -2))
        self.assertRaises(ValueError, lambda: dijkstra(g, 1))
        g2 = Graph(True)
        g2.add_node(1)
        g2.add_node(2)
        g2.add_edge(Edge(1, 2, 'a'))
        self.assertRaises(ValueError, lambda: dijkstra(g2, 1))

    def test_dijkstra1(self):
        g = Graph(True)
        g.add_node("A")
        g.add_node("B")
        g.add_node("C")
        g.add_node("D")
        g.add_edge(Edge("A", "B", 3))
        g.add_edge(Edge("B", "C", 5))
        g.add_edge(Edge("C", "A", 7))
        result = dijkstra(g, "A")
        self.assertEqual(result,
                         ({'A': 0, 'B': 3, 'C': 8, 'D': float("inf")}, {'A': None, 'B': 'A', 'C': 'B', 'D': None}))
        result = dijkstra(g, "B")
        self.assertEqual(result,
                         ({'A': 12, 'B': 0, 'C': 5, 'D': float("inf")}, {'B': None, 'A': 'C', 'C': 'B', 'D': None}))
        result = dijkstra(g, "C")
        self.assertEqual(result,
                         ({'A': 7, 'B': 10, 'C': 0, 'D': float("inf")}, {'B': 'A', 'C': None, 'A': 'C', 'D': None}))
        result = dijkstra(g, "D")
        self.assertEqual(result, ({'A': float("inf"), 'B': float("inf"), 'C': float("inf"), 'D': 0},
                                  {'B': None, 'C': None, 'A': None, 'D': None}))

    def test_dijkstra2(self):
        g = Graph(True)
        for x in range(6):
            g.add_node(x)
        edges = [(0, 5, 3), (3, 0, 7), (2, 3, 4), (3, 5, 2), (5, 2, 1), (4, 3, 9), (2, 4, 2), (3, 1, 5), (2, 5, 4)]
        for e in edges:
            (x, y, z) = e
            g.add_edge(Edge(x, y, z))
        result = dijkstra(g, 0)
        self.assertEqual(result, ({0: 0, 1: 13, 2: 4, 3: 8, 4: 6, 5: 3}, {0: None, 1: 3, 2: 5, 3: 2, 4: 2, 5: 0}))
        result = dijkstra(g, 1)
        self.assertEqual(result, (
        {0: float("inf"), 1: 0, 2: float("inf"), 3: float("inf"), 4: float("inf"), 5: float("inf")},
        {0: None, 1: None, 2: None, 3: None, 4: None, 5: None}))
        result = dijkstra(g, 2)
        self.assertEqual(result, ({0: 11, 1: 9, 2: 0, 3: 4, 4: 2, 5: 4}, {0: 3, 1: 3, 2: None, 3: 2, 4: 2, 5: 2}))
        result = dijkstra(g, 3)
        self.assertEqual(result, ({0: 7, 1: 5, 2: 3, 3: 0, 4: 5, 5: 2}, {0: 3, 1: 3, 2: 5, 3: None, 4: 2, 5: 3}))
        result = dijkstra(g, 4)
        self.assertEqual(result, ({0: 16, 1: 14, 2: 12, 3: 9, 4: 0, 5: 11}, {0: 3, 1: 3, 2: 5, 3: 4, 4: None, 5: 3}))
        result = dijkstra(g, 5)
        self.assertEqual(result, ({0: 12, 1: 10, 2: 1, 3: 5, 4: 3, 5: 0}, {0: 3, 1: 3, 2: 5, 3: 2, 4: 2, 5: None}))

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()  # uruchamia wszystkie testy
