#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from graph import *

# Kod testujący moduł graph.

import unittest


class TestEdge(unittest.TestCase):
    def setUp(self):
        self.e1 = Edge(3, 5, 8)
        self.e2 = Edge(4, 1, 1)
        self.e3 = Edge(4, 1)

    def test_repr(self):
        self.assertEqual(repr(self.e1), "Edge(3, 5, 8)")
        self.assertEqual(repr(self.e2), "Edge(4, 1)")
        self.assertEqual(repr(self.e3), "Edge(4, 1)")

    def test_equal(self):
        self.assertTrue(self.e1 == self.e1)
        self.assertTrue(self.e2 == self.e3)
        self.assertFalse(self.e1 == self.e2)
        self.assertTrue(self.e1 != self.e2)
        self.assertFalse(self.e1 != self.e1)
        self.assertFalse(self.e2 != self.e3)

    def test_invert(self):
        self.assertEqual(~self.e1, Edge(5, 3, 8))
        self.assertEqual(~self.e2, Edge(1, 4))

    def tearDown(self): pass


class TestGraph(unittest.TestCase):
    def setUp(self):
        pass

    def test_addNode(self):
        g = Graph()
        self.assertEqual(g.v(), 0)
        w = [3, 5, 8, 11, 13]
        for el in w:
            self.assertTrue(g.add_node(el))
            self.assertFalse(g.add_node(el))
        self.assertEqual(g.v(), len(w))
        for el in w:
            self.assertTrue(g.has_node(el))
        self.assertFalse(g.has_node(-100))

    def test_addEdge_Directed(self):
        g = Graph(True)
        self.assertEqual(g.e(), 0)
        g.add_node(1)
        g.add_node(2)
        g.add_node(3)
        w = [(1, 2, 5), (2, 1, 3), (3, 1, 4)]
        for el in w:
            (e1, e2, wght) = el
            e = Edge(e1, e2, wght)
            self.assertTrue(g.add_edge(e))
            self.assertFalse(g.add_edge(e))
        self.assertEqual(g.e(), len(w))
        for el in w:
            (e1, e2, wght) = el
            e = Edge(e1, e2, wght)
            self.assertTrue(g.has_edge(e))
        self.assertFalse(g.has_edge(Edge(1, 3)))
        self.assertFalse(g.add_edge(Edge(5, 1, 3)))
        self.assertFalse(g.add_edge(Edge(2, 6, 3)))

    def test_addEdge_Undirected(self):
        g = Graph(False)
        self.assertEqual(g.e(), 0)
        g.add_node(1)
        g.add_node(2)
        g.add_node(3)
        g.add_node(4)
        w = [(1, 2, 5), (4, 1, 3), (3, 1, 4), (1, 1, 0)]
        for el in w:
            (e1, e2, wght) = el
            e = Edge(e1, e2, wght)
            self.assertTrue(g.add_edge(e))
            self.assertFalse(g.add_edge(e))
            self.assertFalse(g.add_edge(~e))
        self.assertEqual(g.e(), len(w))
        for el in w:
            (e1, e2, wght) = el
            e = Edge(e1, e2, wght)
            self.assertTrue(g.has_edge(e))
            self.assertTrue(g.has_edge(~e))
        self.assertFalse(g.add_edge(Edge(5, 1, 3)))
        self.assertFalse(g.add_edge(Edge(2, 6, 3)))

    def test_delEdge_Directed(self):
        g = Graph(True)
        self.assertEqual(g.e(), 0)
        g.add_node(1)
        g.add_node(2)
        g.add_node(3)
        w = [(1, 2, 5), (2, 1, 3), (3, 1, 4), (2, 2, 4)]
        for el in w:
            (e1, e2, wght) = el
            e = Edge(e1, e2, wght)
            g.add_edge(e)
        w2 = [(1, 2, 5), (2, 1, 3), (2, 2, 4)]
        for el in w2:
            (e1, e2, wght) = el
            e = Edge(e1, e2, wght)
            self.assertTrue(g.has_edge(e))
            self.assertTrue(g.del_edge(e))
            self.assertFalse(g.has_edge(e))
        self.assertEqual(g.e(), 1)
        self.assertTrue(g.has_edge(Edge(3, 1)))

    def test_delEdge_Undirected(self):
        g = Graph(False)
        self.assertEqual(g.e(), 0)
        g.add_node(1)
        g.add_node(2)
        g.add_node(3)
        g.add_node(4)
        w = [(1, 2, 5), (4, 1, 3), (3, 1, 4), (2, 2, 4)]
        for el in w:
            (e1, e2, wght) = el
            e = Edge(e1, e2, wght)
            g.add_edge(e)
        w2 = [(1, 2, 5), (3, 1, 3), (2, 2, 4)]
        for el in w2:
            (e1, e2, wght) = el
            e = Edge(e1, e2, wght)
            self.assertTrue(g.has_edge(e))
            self.assertTrue(g.has_edge(~e))
            self.assertTrue(g.del_edge(e))
            self.assertFalse(g.has_edge(e))
            self.assertFalse(g.has_edge(~e))
        self.assertEqual(g.e(), 1)
        self.assertTrue(g.has_edge(Edge(4, 1)))
        self.assertTrue(g.has_edge(Edge(1, 4)))

    def test_delNode(self):
        g = Graph()
        g.add_node(1)
        g.add_node(2)
        self.assertEqual(g.v(), 2)
        self.assertTrue(g.del_node(1))
        self.assertFalse(g.del_node(1))
        self.assertFalse(g.del_node(4))
        self.assertEqual(g.v(), 1)

        g = Graph()
        g.add_node(1)
        g.add_node(2)
        g.add_node(3)
        g.add_node(4)
        w = [(1, 3, 1), (3, 4, 2), (1, 4, 2), (3, 3, 0)]
        for el in w:
            (e1, e2, wght) = el
            e = Edge(e1, e2, wght)
            g.add_edge(e)
        self.assertTrue(g.del_node(3))
        self.assertFalse(g.del_node(3))
        self.assertEqual(g.v(), 3)
        self.assertEqual(g.e(), 1)
        self.assertTrue(g.has_edge(Edge(1, 4)))

    def test_weight(self):
        g = Graph(False)
        g.add_node(1)
        g.add_node(2)
        g.add_edge(Edge(1, 2, 3))
        g.add_edge(Edge(1, 1, 5))
        self.assertEqual(g.weight(Edge(1, 2)), 3)
        self.assertEqual(g.weight(Edge(1, 1)), 5)
        self.assertEqual(g.weight(Edge(1, 4)), None)
        self.assertEqual(g.weight(Edge(4, 2)), None)

    def test_copy(self):
        g1 = Graph()
        g1.add_node(1)
        g1.add_node(2)
        g2 = g1.copy()
        self.assertEqual(g1.v(), 2)
        self.assertEqual(g2.v(), 2)
        g1.add_node(4)
        g2.del_node(2)
        self.assertEqual(g1.v(), 3)
        self.assertEqual(g2.v(), 1)

    def test_transpose(self):
        g = Graph(True)
        g.add_node(1)
        g.add_node(2)
        g.add_node(3)
        g.add_node(4)
        edgs = [Edge(1, 2, 4), Edge(3, 1, 2), Edge(4, 2, 1)]
        for elem in edgs:
            g.add_edge(elem)
        g2 = g.transpose()
        self.assertEqual(g.e(), g2.e())
        self.assertEqual(g.v(), g2.v())
        for elem in edgs:
            self.assertTrue(g2.has_edge(~elem))
            self.assertFalse(g2.has_edge(elem))

    def tearDown(self):
        pass


class TestIterator(unittest.TestCase):
    def setUp(self):
        self.g = Graph(True)
        self.g.add_node(1)
        self.g.add_node(2)
        self.g.add_node(3)
        self.g.add_node(4)
        self.g.add_edge(Edge(1, 3, 2))
        self.g.add_edge(Edge(2, 3, 4))
        self.g.add_edge(Edge(1, 4, 1))
        self.g.add_edge(Edge(3, 1, 1))
        self.g.add_edge(Edge(3, 2, 4))
        self.g.add_edge(Edge(4, 1, 5))
        self.g.add_edge(Edge(3, 3, 0))

    def test_iternodes(self):
        n = [1, 2, 3, 4]
        it = self.g.iternodes()
        il = 0
        while it.hasNext():
            elem = it.get()
            self.assertTrue(elem in n)
            il += 1
        self.assertEqual(len(n), il)

    def test_iteradjacent(self):
        n = [1, 2, 3]
        it = self.g.iteradjacent(3)
        il = 0
        while it.hasNext():
            elem = it.get()
            self.assertTrue(elem in n)
            il += 1
        self.assertEqual(len(n), il)

    def test_iteroutedges(self):
        ed = [(3, 1), (3, 2), (3, 3)]
        it = self.g.iteroutedges(3)
        il = 0
        while it.hasNext():
            elem = it.get()
            self.assertTrue((elem.source, elem.target) in ed)
            il += 1
        self.assertEqual(len(ed), il)

    def test_iterinedges(self):
        ed = [(1, 3), (2, 3), (3, 3)]
        it = self.g.iterinedges(3)
        il = 0
        while it.hasNext():
            elem = it.get()
            self.assertTrue((elem.source, elem.target) in ed)
            il += 1
        self.assertEqual(len(ed), il)

    def test_iteredges(self):
        ed = [(1, 3), (2, 3), (3, 3), (1, 4), (4, 1), (3, 1), (3, 2)]
        it = self.g.iteredges()
        il = 0
        while it.hasNext():
            elem = it.get()
            self.assertTrue((elem.source, elem.target) in ed)
            il += 1
        self.assertEqual(len(ed), il)

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()  # uruchamia wszystkie testy
