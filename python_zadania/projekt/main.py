#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from graph import *
from graphfromfile import *
from dijkstra import *

#plik z ktorego jest wczytywany graf
graphFilename = "graf.txt"

g = createGraphFromFile(graphFilename)
print()
print("===Wczytany graf:===")
g.print_graph()
print()
print("===Algorytm dijkstry:===")
dijkstra(g, "A", True)