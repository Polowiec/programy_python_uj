#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from graph import *


# jesli sie da konwersja tekstu na liczbe
def tryToNum(s):
    try:
        fl = float(s)
        if fl == int(fl):
            return int(fl)
        else:
            return fl
    except ValueError:
        return s;


def createGraphFromFile(fileName, directed=True):
    file = open(fileName, 'r')
    g = Graph(directed)
    edgesToAdd = []
    for line in file:
        line = line.strip().replace("\n", "")
        if len(line) < 1:
            continue

        # dodawanie wierzcholkow, jak sie uda to skonwertowanie na inta lub floata
        spl = line.split(":")
        w = tryToNum(spl[0].strip())
        if not g.add_node(w):
            print("Nie dodano wierzcholka: %s - już istnieje" % w)

        w2 = spl[1]  # reszta
        if len(w2) < 1:
            continue
        row = w2.split(";")
        for elem in row:  # krawedzie docelowe
            if len(elem) < 1:
                continue
            wart = elem.strip().split(",")  # rozbicie na target i wage
            w_doc = tryToNum(wart[0].strip())
            w_weight = tryToNum(wart[1].strip())
            edgesToAdd.append(Edge(w, w_doc, w_weight))
    file.close()

    # dodawanie krawedzi do grafu
    for e in edgesToAdd:
        weight = e.weight
        if isinstance(weight, (int, float)):
            if weight >= 0:
                if not g.add_edge(e):
                    print("Nie dodano krawedzi: %s - krawedz istnieje lub brak wierzchołka" % e)
            else:
                print("Nie dodano krawedzi: %s - waga jest ujemna" % e)
        else:
            print("Nie dodano krawedzi: %s - waga nie jest liczbą" % e)

    return g
