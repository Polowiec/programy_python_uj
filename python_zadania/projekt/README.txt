Autor: Tomasz Polowiec
Projekt: Algorytm Dijkstry

Program realizuje dzia�anie algorytmu Dijkstry. Algorytm ten w grafie o nieujemnych wagach znajduje najkr�tsze �ciezki z wierzcho�ka pocz�tkowego do wszystkich pozosta�ych ( o ile takie istniej� ).

Projekt sk��da si� z nast�puj�cych plik�w:
graph.py - zawiera klasy graph, edges i iterator dzieki ktorym mozliwe jest tworzenie grafu, przegl�danie go i jego modyfikacja
graphfromfile.py - posiada funkcje do �adowania danych z pliku tekstowego do grafu wraz z podstawow� walidajc� danych
dijkstra.py - modu� z funkcj� realizuj�c� algorytm dijkstry
main.py - plik startowy projektu do obs�ugi projektu
tests_dijkstra.py, tests_graph.py - modu�y testuj�ce

Instrukcja:
Nale�y uruchomi� plik main.py. W pliku main.py.
W zmiennej "graphFilename" znajduje si� �cie�ka wzgl�dna do pliku z kt�rego zostanie za�adowany graf.
Dane w pliku z grafem musz� posiada� nast�puj�cy format:
- ka�da linijka musi zaczyna� si� od nazwy wierzcho�ka i dwukropka: "nazwa:"
- po nazwie wierzcholka wyst�puj� kolejne kraw�dzie wychodz�ce z tego wierzcho�ka
- ka�da kraw�d� sk�ada si� z wierzcho�ka docelowego i wagi
- wierzcho�ek docelowy od wagi musi by� oddzielony przecinkiem, a ka�da kraw�d� zako�czona �rednikiem: "wierzcho�ek, waga;"
- je�liz  danego wierzcho�ka nie wychodzi �adna kraw�d� musi si� znalesc linijka z jego nazw�
- krawed� zawieraj�ca wierzcho�ek dla kt�rego nie ma linii nie b�dzie akceptowana

Przyk�ad pliku:

A: B, 3; C: 2;
B:
C: A, 2;