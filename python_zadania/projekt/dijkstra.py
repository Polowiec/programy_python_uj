#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


# sprawdza czy krawedzie w grafie maja dopuszczalne wagi ( nieujemne liczby )
def checkEdges(g):
    it = g.iteredges()
    while it.hasNext():
        edg = it.get()
        if isinstance(edg.weight, (int, float)):
            if edg.weight < 0:
                return False
        else:
            return False
    return True


# wykonuje wlasciwy algorytm dijkstry, przyjmuje jakjo argumenty graf i wierzcholek startowy
# zwraca krotke zawierajaca dystanse, oraz odwzorowanie z jakich wierzcholkow da sie dojsc do pozostalych
def dijkstra(g, startNode, printResult=False):
    if not checkEdges(g):
        raise ValueError("Graf zawiera niepoprawne wagi krawędzi")
    if not g.has_node(startNode):
        raise ValueError("Nie istnieje wierzchołek %s w grafie" % startNode)
    parents = {}
    heap = []
    visited = {}
    pos = {}
    dist = {}
    heapSize = 0

    it = g.iternodes()
    # inicjacja struktur z danymi
    while it.hasNext():
        w = it.get()
        dist[w] = float("inf")
        visited[w] = False
        parents[w] = None
        heap.append(w)
        pos[w] = heapSize
        heapSize += 1

    # ustawienie parametrow startowych
    dist[startNode] = 0
    p = pos[startNode]
    # ustawienie elementu startowego na poczatek stogu
    heap[p], heap[0] = heap[0], heap[p]
    pos[startNode] = 0
    pos[heap[p]] = p

    for k in range(heapSize):
        curr_node = deleteMinimum(heap, heapSize, dist, pos)  # sciagniecie wierzcholka z minimalnym dist
        heapSize -= 1
        visited[curr_node] = True
        it = g.iteroutedges(curr_node)
        while it.hasNext():  # po wszsytkich krawedziach wychodzacych
            e = it.get()
            if not visited[e.target]:
                if dist[curr_node] != float("inf"):
                    if dist[curr_node] + e.weight < dist[e.target]:  # optymalniejsza trasa
                        dist[e.target] = dist[curr_node] + e.weight
                        parents[e.target] = curr_node
                        upHeap(heap, pos[e.target], dist, pos)  # odświeżenie kolejki priorytetowej

    if printResult:
        printDijkstraResult(startNode, dist, parents)
    return (dist, parents)


# wyciagniecie elementu o najmniejszej wadze
def deleteMinimum(heap, heapSize, dist, pos):
    tmp = heap[0]
    heap[0] = heap[heapSize - 1]
    downHeap(heap, 0, heapSize - 2, dist, pos)
    return tmp


# przesiewanie w dol
def downHeap(heap, start, end, dist, pos):
    tmp = heap[start]
    j = 2 * start + 1
    while j <= end:
        if j + 1 <= end:  # istnieje prawe dziecko
            if dist[heap[j]] > dist[heap[j + 1]]:  # wybor mniejszego dziecka
                j += 1

        if dist[heap[j]] > dist[tmp]:  # dziecko wieksze koniec przesiewania
            break
        # przeskoczenie dzieckia w hierarhii o 1 w gore
        heap[start] = heap[j]
        pos[heap[start]] = start
        start = j
        j = 2 * start + 1

    heap[start] = tmp
    pos[tmp] = start


# przesiewanie w góre
def upHeap(heap, start, dist, pos):
    tmp = heap[start]
    j = int((start - 1) / 2)

    while start > 0 and dist[heap[j]] > dist[tmp]:
        heap[start] = heap[j]
        pos[heap[start]] = start
        start = j
        j = int((start - 1) / 2)

    heap[start] = tmp
    pos[tmp] = start


def printDijkstraResult(startNode, dist, parents):
    print("Wierzchołek startowy %s" % startNode)
    for d in dist:
        wiersz = str(d).rjust(3) + ":  "
        if dist[d] == float("inf"):
            wiersz += "Wierzchołek nieosiągalny"
        else:
            sciezka = str(d)
            w = d
            sciezka = str(w)
            while parents[w] != None:
                sciezka = str(parents[w]) + "-->" + sciezka
                w = parents[w]
            wiersz += sciezka + "   dist= " + str(dist[d])
        print(wiersz)
