#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


class Edge:
    def __init__(self, source, target, weight=1):
        self.source = source
        self.target = target
        self.weight = weight

    def __repr__(self):
        if self.weight == 1:
            return "Edge(%s, %s)" % (
                repr(self.source), repr(self.target))
        else:
            return "Edge(%s, %s, %s)" % (
                repr(self.source), repr(self.target), repr(self.weight))

    def __eq__(self, other):
        return self.source == other.source and self.target == other.target and self.weight == other.weight

    def __ne__(self, other):
        return not self == other

    def __hash__(self):
        return hash(repr(self))

    def __invert__(self):
        return Edge(self.target, self.source, self.weight)


class Iterator:
    def __init__(self):
        self.content = []
        self.position = 0
        self.size = 0

    def hasNext(self):
        return self.position < self.size

    def get(self):
        if self.position >= self.size:
            raise ValueError
        self.position += 1
        return self.content[self.position - 1]


# Klasa graf, moze zachowywać się jako graf skierowany lub nie
# Klasa dopuszcza krawędzie będące pętlami

class Graph:
    def __init__(self, directed=False):
        self.adjacencyList = {}
        self.directed = directed

    def v(self):  # zwraca liczbę wierzchołków
        return len(self.adjacencyList)

    def e(self):  # zwraca liczbę krawędzi
        if self.directed:
            return sum(len(self.adjacencyList[node]) for node in self.adjacencyList)
        else:
            suma = sum(len(self.adjacencyList[node]) for node in self.adjacencyList)
            for x in self.adjacencyList:
                if x in self.adjacencyList[x]:
                    suma += 1
            return int(suma / 2)

    def is_directed(self):  # bool, czy graf skierowany
        return self.directed

    def add_node(self, node):  # dodaje wierzchołek
        if node not in self.adjacencyList:
            self.adjacencyList[node] = {}
            return True
        return False

    def has_node(self, node):  # bool
        return node in self.adjacencyList

    def del_node(self, node):  # usuwa wierzchołek
        if not node in self.adjacencyList:
            return False
        del self.adjacencyList[node]  # usuniecie wierzcholka i krawedzi wychodzacych
        toRemove = []
        for elem in self.adjacencyList:
            for x in self.adjacencyList[elem]:
                if node == x:
                    toRemove.append(elem)
        for x in toRemove:
            del self.adjacencyList[x][node]  # usuniecie krawedzi przychodzacych
        return True

    def add_edge(self, edge):  # wstawienie krawędzi
        if edge.source in self.adjacencyList:
            if edge.target in self.adjacencyList[edge.source]:
                return False  # istnieje juz krawedz pomiedzy wierzcholkami
        else:
            return False  # nie istnieje wierzcholek
        if not edge.target in self.adjacencyList:
            return False  # nie istnieje drugi wierzcholek

        self.adjacencyList[edge.source][edge.target] = edge.weight
        if not self.directed and edge.source != edge.target:
            self.adjacencyList[edge.target][edge.source] = edge.weight
        return True

    def has_edge(self, edge):  # bool
        if edge.source in self.adjacencyList:
            if edge.target in self.adjacencyList[edge.source]:
                return True
        return False

    def del_edge(self, edge):  # usunięcie krawędzi
        if self.has_edge(edge):
            del self.adjacencyList[edge.source][edge.target]
            if not self.directed and edge.source != edge.target:
                del self.adjacencyList[edge.target][edge.source]
            return True
        return False

    def weight(self, edge):  # zwraca wagę krawędzi
        if self.has_edge(edge):
            return self.adjacencyList[edge.source][edge.target]
        return None

    def iternodes(self):  # iterator po wierzchołkach
        it = Iterator()
        for x in self.adjacencyList:
            it.size += 1
            it.content.append(x)
        return it

    def iteradjacent(self, node):  # iterator po wierzchołkach sąsiednich
        it = Iterator()
        if node in self.adjacencyList:
            for x in self.adjacencyList[node]:
                it.size += 1
                it.content.append(x)
        return it

    def iteroutedges(self, node):  # iterator po krawędziach wychodzących
        it = Iterator()
        if node in self.adjacencyList:
            for x in self.adjacencyList[node]:
                it.size += 1
                it.content.append(Edge(node, x, self.adjacencyList[node][x]))
        return it

    def iterinedges(self, node):  # iterator po krawędziach przychodzących
        it = Iterator()
        for elem in self.adjacencyList:
            for x in self.adjacencyList[elem]:
                if x == node:
                    it.size += 1
                    it.content.append(Edge(elem, node, self.adjacencyList[elem][node]))
        return it

    def iteredges(self):  # iterator po krawędziach
        it = Iterator()
        for elem in self.adjacencyList:
            for x in self.adjacencyList[elem]:
                if self.directed or elem < x:
                    it.size += 1
                    it.content.append(Edge(elem, x, self.adjacencyList[elem][x]))
        return it

    def copy(self):  # zwraca kopię grafu
        newGraph = Graph(self.directed)
        newGraph.adjacencyList = dict(self.adjacencyList)
        return newGraph

    def transpose(self):  # zwraca graf transponowany
        if self.directed == False:
            return self.copy()
        trans = Graph(True)
        for x in self.adjacencyList:
            trans.add_node(x)
        for elem in self.adjacencyList:
            for x in self.adjacencyList[elem]:
                trans.add_edge(Edge(x, elem, self.adjacencyList[elem][x]))
        return trans

    def print_graph(self, showWeight=True):
        for node in self.adjacencyList:
            wiersz = str(node) + ": "
            for elem in self.adjacencyList[node]:
                if showWeight:
                    wiersz += "-%s->%s " % (self.adjacencyList[node][elem], elem)
                else:
                    wiersz += "-->%s" % elem
            print(wiersz)
