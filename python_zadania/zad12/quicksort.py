#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


def quicksort(L, left, right):
    if left >= right:
        return
    swap(L, left, int((left + right) / 2))  # element podziału
    last = left  # przesuń do L[left]
    for i in range(left + 1, right + 1):  # podział
        if L[i] < L[left]:
            last = last + 1
            swap(L, last, i)
    swap(L, left, last)  # odtwórz element podziału
    quicksort(L, left, last - 1)
    quicksort(L, last + 1, right)


def swap(L, i, k):
    L[i], L[k] = L[k], L[i]
