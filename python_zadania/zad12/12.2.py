#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from binarysearch import *

L = [1, 3, 4, 5, 9, 12, 14, 55]
print(binarne_rek(L, 0, len(L) - 1, 4))
print(binarne_rek(L, 0, len(L) - 1, 55))
print(binarne_rek(L, 0, len(L) - 1, 9))
print(binarne_rek(L, 0, len(L) - 1, 18))
