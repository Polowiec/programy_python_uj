#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


from quicksort import *
from moda import *
from generatelist import *

def moda_sort(L, left, right):
    if right < left:
        raise ValueError
    quicksort(L, left, right)
    #print(L)
    w= moda(L, left, right)
    if w == None:
        return None
    else:
        return L[w]

L =powtarzajaceZZakresuLista(20, 5)
print(L)
print(moda_sort(L, 0, 19))