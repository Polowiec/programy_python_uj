#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from quicksort import *
from generatelist import *


def mediana_sort(L, left, right):
    if right < left:
        raise ValueError
    quicksort(L, left, right)
    ilosc = right - left + 1
    srodek = int((left + right) / 2)
    if ilosc % 2 == 0:
        # srednia elementow
        return (L[srodek] + L[srodek + 1]) / 2
    else:
        return L[srodek]


L = powtarzajaceZZakresuLista(10, 5)
print(L)
print(mediana_sort(L, 0, 9))
print()

L = powtarzajaceZZakresuLista(9, 5)
print(L)
print(mediana_sort(L, 0, 8))
