#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from generatelist import *
from linearsearch import *
import random

ilosc = 100
zakres = 10
L = powtarzajaceZZakresuLista(ilosc, zakres)
print(L)

y = random.randint(0, zakres - 1)
print("Liczba " + str(y) + " wystepuje na pozycjach:")

start = 0
while start < ilosc:
    poz = linear_search(L, start, ilosc - 1, y)
    if poz == None:
        break
    print(poz)
    start = poz + 1
