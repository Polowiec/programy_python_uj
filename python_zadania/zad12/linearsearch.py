#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


def linear_search(L, left, right, y):
    """Wyszukiwanie liniowe w ciągu."""
    if right < left:
        raise ValueError
    i = left
    while i <= right:
        if y == L[i]:
            return i
        i = i + 1
    return None
