#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from generatelist import *


def lider_py(L, left, right):
    if right < left:
        raise ValueError
    wystapienia = dict()
    for x in range(left, right + 1):
        if L[x] in wystapienia:
            wystapienia[L[x]] += 1
        else:
            wystapienia[L[x]] = 1
    max_elem = -1
    max_ilosc = 0
    for x, y in wystapienia.items():
        if y > max_ilosc:
            max_ilosc = y
            max_elem = x
    if max_ilosc > (right - left + 1) / 2:
        return max_elem
    else:
        return None


L = powtarzajaceZZakresuLista(20, 5)
print(L)
print(lider_py(L, 0, 19))

L = powtarzajaceZZakresuLista(20, 2)
print(L)
print(lider_py(L, 0, 19))
