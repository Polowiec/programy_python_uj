#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


def binarne_rek(L, left, right, y):
    """Wyszukiwanie binarne w wersji rekurencyjnej."""
    if right < left:
        raise ValueError
    if left == right:
        if L[left] == y:
            return left
        else:
            return None

    srodek = int((left + right) / 2)
    wart = L[srodek]
    if wart == y:
        return srodek
    elif wart > y:
        return binarne_rek(L, left, srodek - 1, y)
    else:
        return binarne_rek(L, srodek + 1, right, y)
