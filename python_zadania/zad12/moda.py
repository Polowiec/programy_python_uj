#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


def moda(L, left, right):
    """Wyszukiwanie mody w ciągu."""
    if left+1 > right:
        return None
    i1 = None             # najlepszy kandydat (indeks)
    number1 = 0           # jego liczebność
    i2 = left             # bieżący element (indeks)
    number2 = 1           # jego liczebność
    while i2 < right:
        i2 = i2 + 1
        if L[i2] == L[i2-1]:    # jeżeli się powtórzył
            number2 = number2 + 1
            # na bieżąco uaktualniamy najlepszego kandydata
            if number2 > number1:  # jest lepszy kandydat
                number1 = number2
                i1 = i2
        else:                   # nowy bieżący element
            number2 = 1
    return i1