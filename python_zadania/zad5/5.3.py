#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from polys import *

print(add_poly([1, 2, 3], [-1, 4, -7]))
print(sub_poly([1, 2, 3], [-1, 4, -7]))
print(mul_poly([1, 2, 3], [-1, 4, -7]))
print(is_zero([1, 2, 3]))
print(cmp_poly([1, 2, 3], [1, 2, 3]))
print(eval_poly([1, 2, 3], 3))
print(combine_poly([5, 6, 2], [1, 1]))
print(pow_poly([3, 1], 3))
print(diff_poly([5, 3, 2]))
