#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

from math import gcd


def normujUlamek(frac):
    if frac[0] == 0:
        return [0, 1]

    if frac[1] < 0:
        frac[0] *= -1
        frac[1] *= -1

    return frac


def add_frac(frac1, frac2):  # frac1 + frac2
    wsp_mian = frac2[1] * frac1[1]
    licznik = frac1[0] * frac2[1] + frac2[0] * frac1[1]
    skrocenie = gcd(licznik, wsp_mian)

    return normujUlamek([int(licznik / skrocenie), int(wsp_mian / skrocenie)])


def sub_frac(frac1, frac2):  # frac1 - frac2
    wsp_mian = frac2[1] * frac1[1]
    licznik = frac1[0] * frac2[1] - frac2[0] * frac1[1]
    skrocenie = gcd(licznik, wsp_mian)

    return normujUlamek([int(licznik / skrocenie), int(wsp_mian / skrocenie)])


def mul_frac(frac1, frac2):  # frac1 * frac2
    mian = frac2[1] * frac1[1]
    licznik = frac1[0] * frac2[0]
    skrocenie = gcd(licznik, mian)

    return normujUlamek([int(licznik / skrocenie), int(mian / skrocenie)])


def div_frac(frac1, frac2):  # frac1 / frac2
    mian = frac2[0] * frac1[1]
    if mian == 0:
        return []
    licznik = frac1[0] * frac2[1]
    skrocenie = gcd(licznik, mian)

    return normujUlamek([int(licznik / skrocenie), int(mian / skrocenie)])


def cmp_frac(frac1, frac2):  # -1 | 0 | +1
    frac1 = normujUlamek(frac1)
    frac2 = normujUlamek(frac2)
    l1 = frac1[0] * frac2[1]
    l2 = frac2[0] * frac1[1]

    if l1 < l2:
        return -1
    elif l1 == l2:
        return 0
    else:
        return 1


def is_positive(frac):  # bool, czy dodatni
    return (frac[0] * frac[1] > 0)


def is_zero(frac):  # bool, typu [0, x]
    return (frac[0] == 0)


def frac2float(frac):  # konwersja do float
    return 1.0 * frac[0] / frac[1]


import unittest


class TestFractions(unittest.TestCase):
    def setUp(self):
        self.zero = [0, 1]

    def test_add_frac(self):
        self.assertEqual(add_frac([1, 2], [1, 3]), [5, 6])
        self.assertEqual(add_frac([-1, -2], [1, 3]), [5, 6])
        self.assertEqual(add_frac([-1, 2], [1, -3]), [-5, 6])
        self.assertEqual(add_frac([-1, 2], [1, 4]), [-1, 4])
        self.assertEqual(add_frac([-1, 6], [1, 6]), self.zero)
        self.assertEqual(add_frac([-1, 6], [3, 9]), [1, 6])

    def test_sub_frac(self):
        self.assertEqual(sub_frac([1, 2], [1, 3]), [1, 6])
        self.assertEqual(sub_frac([1, 2], [-1, 3]), [5, 6])
        self.assertEqual(sub_frac([2, 3], [1, 1]), [-1, 3])
        self.assertEqual(sub_frac([2, 3], self.zero), [2, 3])
        self.assertEqual(sub_frac(self.zero, [2, 3]), [-2, 3])

    def test_mul_frac(self):
        self.assertEqual(mul_frac([1, 2], [1, 3]), [1, 6])
        self.assertEqual(mul_frac([1, 2], [-1, 3]), [-1, 6])
        self.assertEqual(mul_frac([1, 2], [1, -3]), [-1, 6])
        self.assertEqual(mul_frac([-1, 2], [1, -3]), [1, 6])
        self.assertEqual(mul_frac(self.zero, [1, -3]), self.zero)
        self.assertEqual(mul_frac([1, 2], [2, 1]), [1, 1])

    def test_div_frac(self):
        self.assertEqual(div_frac([1, 2], [1, 3]), [3, 2])
        self.assertEqual(div_frac([-1, 2], [1, 3]), [-3, 2])
        self.assertEqual(div_frac([1, -2], [1, 3]), [-3, 2])
        self.assertEqual(div_frac([-1, 2], [-1, 3]), [3, 2])
        self.assertEqual(div_frac([1, -2], [1, -3]), [3, 2])
        self.assertEqual(div_frac([1, 8], [-9, 1]), [-1, 72])
        self.assertEqual(div_frac([1, 8], [1, 8]), [1, 1])
        self.assertEqual(div_frac(self.zero, [1, 8]), self.zero)

    def test_cmp_frac(self):
        self.assertEqual(cmp_frac([1, 2], [1, 4]), 1)
        self.assertEqual(cmp_frac([-1, 2], [1, 4]), -1)
        self.assertEqual(cmp_frac([1, 2], [2, 4]), 0)
        self.assertEqual(cmp_frac([-1, 2], [2, -4]), 0)

    def test_is_positive(self):
        self.assertEqual(is_positive([1, 2]), True)
        self.assertEqual(is_positive(self.zero), True)
        self.assertEqual(is_positive([-1, 2]), False)
        self.assertEqual(is_positive([-1, -2]), True)

    def test_frac2float(self):
        self.assertEqual(frac2float([1, 2]), 0.5)
        self.assertEqual(frac2float([1, 4]), 0.25)
        self.assertEqual(frac2float([1, 3]), 1.0 / 3)
        self.assertEqual(frac2float([-1, 3]), -1.0 / 3)
        self.assertEqual(frac2float([1, -3]), -1.0 / 3)

    def tearDown(self): pass


if __name__ == '__main__':
    unittest.main()  # uruchamia wszystkie testy
