#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

def usun_zera(sparse):
    doUsuniecia= list()
    for (k1, k2), v in sparse.items():
        if v == 0:
            doUsuniecia.append( (k1, k2) )
    for x in doUsuniecia:
        del sparse[x]
    return sparse


def add_sparse(sparse1, sparse2):       # sparse1 + sparse2
    wynik= dict(sparse1)
    for (k1, k2), v in sparse2.items():
        if (k1, k2) in wynik:
            wynik[(k1, k2)]+= v
        else:
            wynik[(k1, k2)]= v
    return usun_zera(wynik)


def sub_sparse(sparse1, sparse2):        # sparse1 - sparse2
    wynik= dict(sparse1)
    for (k1, k2), v in sparse2.items():
        if (k1, k2) in wynik:
            wynik[(k1, k2)]-= v
        else:
            wynik[(k1, k2)]= -v
    return usun_zera(wynik)


def mul_sparse(sparse1, sparse2):        # sparse1 * sparse2
    wynik= dict()
    for (k1, k2), v1 in sparse1.items():
        for(k3, k4), v2 in sparse2.items():
            if k2 == k3:
                if (k1,k4) in wynik:
                    wynik[(k1, k4)]+= v1*v2
                else:
                    wynik[(k1, k4)]= v1*v2
    return usun_zera(wynik)


def is_diagonal(sparse):                 # bool, czy diagonalna
    for (k1, k2), v in sparse.items():
        if k1 != k2 and v != 0:
            return False
    return True


def is_empty(sparse):                    # bool, czy zerowa
    return usun_zera(sparse) == {}

import unittest

class TestSparseMatrices(unittest.TestCase):
    def setUp(self):
        self. zero = {}                   # macierz zerowa

    def test_addsparsematrices(self):
        self.assertEqual(add_sparse(self.zero, self.zero), self.zero)
        self.assertEqual(add_sparse({(1,1):4}, {(1,1):-4}), self.zero)
        self.assertEqual(add_sparse({(1,1):4, (0, 1): 2}, {(1,1):2, (1, 0): 3}), {(1,1):6, (0,1): 2, (1,0): 3})

    def test_subsparsematrices(self):
        self.assertEqual(sub_sparse(self.zero, self.zero), self.zero)
        self.assertEqual(sub_sparse({(1,1):4}, {(1,1):4}), self.zero)
        self.assertEqual(sub_sparse({(1,1):2, (0, 1): 2}, {(1,1):2, (1, 0): 3}), {(0,1): 2, (1,0): -3})

    def test_mulsparse(self):
        self.assertEqual(mul_sparse(self.zero, self.zero), self.zero)
        self.assertEqual(mul_sparse(self.zero, {(1,1):2, (0, 1): 2}), self.zero)
        self.assertEqual(mul_sparse({(5,5): 1}, {(1,1):2, (0, 1): 2}), self.zero)
        self.assertEqual(mul_sparse({(0,0): 1, (1, 1): 1}, {(1,1):2, (0, 1): 2}), {(1,1):2, (0, 1): 2})

    def test_isdiagonal(self):
        self.assertEqual(is_diagonal(self.zero), True)
        self.assertEqual(is_diagonal({(2,2):2, (4,4):3}), True)
        self.assertEqual(is_diagonal({(2,2):2, (4,4):3, (5, 1): 0}), True)
        self.assertEqual(is_diagonal({(2,2):2, (4,4):3, (5, 1): 8}), False)

    def test_isempty(self):
        self.assertEqual(is_empty({}), True)
        self.assertEqual(is_empty({(1, 1): 0, (300, 300): 0}), True)
        self.assertEqual(is_empty({(3,1):1}), False)

    def tearDown(self): pass


if __name__ == '__main__':
    unittest.main()  # uruchamia wszystkie testy

