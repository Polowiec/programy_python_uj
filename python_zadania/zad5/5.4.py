#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from sparsepolys import *

print(add_sparsepoly({10: 1, 0: 2}, {100: 4, 5: 3, 0: 3}))
print(sub_sparsepoly({10: 1, 0: 2}, {100: 4, 5: 3, 0: 3}))
print(mul_sparsepoly({2: 1, 0: 1}, {2: 1, 0: -1}))
print(is_zero({0: 0, 5: 0, 100: 0}))
print(cmp_sparsepolys({1: 2, 3: 4}, {3: 4, 1: 3}))
print(pow_sparsepoly({0: -1, 1: 1}, 2))
print(combine_sparsepoly({0: 5, 1: 6, 2: 2}, {2: 1}))
print(diff_sparsepoly({0: 12, 1: 2, 3: 4}))
