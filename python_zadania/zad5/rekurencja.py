#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


def fibonacci(n):
    if n < 0:
        return -1

    if n == 0:
        return 0

    poprz = 0
    akt = 1
    for x in range(n-1):
        akt, poprz = akt + poprz, akt
    return akt


def factorial(n):
    if n < 0:
        return -1

    silnia = 1
    while n > 1:
        silnia *= n
        n -= 1
    return silnia
