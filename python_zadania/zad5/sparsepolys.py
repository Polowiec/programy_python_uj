#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

def normalizujSlownik(poly):
    doUsuniecia = list()
    for key, value in poly.items():
        if value == 0:
            doUsuniecia.append(key)
    #nie mozna usowac ze slownika podczas iterowania po nim
    for x in doUsuniecia:
        del poly[x]

    if poly == {}:
        return {0:0}
    else:
        return poly

def add_sparsepoly(poly1, poly2):
    wynik= dict(poly1)
    for key, value in poly2.items():
        if key in wynik:
            wynik[key]+= value
        else:
            wynik[key]= value
    return normalizujSlownik(wynik)


def sub_sparsepoly(poly1, poly2):
    wynik= dict(poly1)
    for key, value in poly2.items():
        if key in wynik:
            wynik[key]-= value
        else:
            wynik[key]= -value
    return normalizujSlownik(wynik)

def mul_sparsepoly(poly1, poly2):
    wynik= dict()
    for key1, value1 in poly1.items():
        for key2, value2 in poly2.items():
            k = key1+ key2
            v= value1*value2
            if k in wynik:
                wynik[k]+= v
            else:
                wynik[k]= v
    return normalizujSlownik(wynik)

def is_zero(poly):
    return normalizujSlownik(poly) == {0: 0}

def cmp_sparsepolys(poly1, poly2):
    return normalizujSlownik(poly1) == normalizujSlownik(poly2)

def pow_sparsepoly(poly, n):
    if n < 0:
        return {}
    if n == 0:
        return {0: 1}

    wynik= dict(poly)
    for x in range(n-1):
        wynik= mul_sparsepoly(wynik, poly)
    return normalizujSlownik(wynik)

def combine_sparsepoly(poly1, poly2):
    if len(poly1)*len(poly2) == 0:
        return {}

    wynik= dict()
    for key1, value1 in poly1.items():
        wynik= add_sparsepoly( mul_sparsepoly(pow_sparsepoly(poly2, key1), {0: value1}), wynik )
    return normalizujSlownik(wynik)

def diff_sparsepoly(poly):
    if len(poly) == 0:
        return {}

    wynik= dict()
    for key, value in poly.items():
        if key != 0:
            wynik[key-1]= key* value
    if len(wynik) == 0:
        return {0: 0}
    else:
        return wynik

import unittest

class TestSparsePolynomials(unittest.TestCase):
    def setUp(self):
        self. zero = {0: 0}                   # W(x) = 3, wielomian zerowego stopnia

    def test_add_sparsepoly(self):
        self.assertEqual(add_sparsepoly(self.zero, self.zero), self.zero)
        self.assertEqual(add_sparsepoly({10: 1, 0: 2} , {100: 4, 5: 3, 0:3} ), {10: 1, 0: 5, 100: 4, 5: 3})
        self.assertEqual(add_sparsepoly({100: 4, 5: 3, 0:3} , {100: -4, 5: -3, 0:-3} ), self.zero)
        self.assertEqual(add_sparsepoly({10: 1, 0: 2} , {100: 4, 5: 3, 0:-2} ), {10: 1,  100: 4, 5: 3})

    def test_sub_sparsepoly(self):
        self.assertEqual(sub_sparsepoly(self.zero, self.zero), self.zero)
        self.assertEqual(sub_sparsepoly({10: 1, 0: 2} , {100: 4, 5: 3, 0:3} ), {10: 1, 0: -1, 100: -4, 5: -3})
        self.assertEqual(sub_sparsepoly({10: 1, 0: 2} , {100: 4, 5: 3, 0:2} ), {10: 1,  100: -4, 5: -3})
        self.assertEqual(sub_sparsepoly({100: 4, 5: 3, 0:2} , {100: 4, 5: 3, 0:2} ), self.zero)

    def test_mul_sparsepoly(self):
        self.assertEqual(mul_sparsepoly(self.zero, self.zero), self.zero)
        self.assertEqual(mul_sparsepoly({10: 1, 0: 2}, self.zero), self.zero)
        self.assertEqual(mul_sparsepoly({10: 1, 0: 2}, {0: 4}), {10: 4, 0: 8})
        self.assertEqual(mul_sparsepoly({2: 1, 0: 1}, {2: 1, 0: -1}), {4: 1, 0: -1})

    def test_is_zero(self):
        self.assertEqual(is_zero(self.zero), True)
        self.assertEqual(is_zero({0: 0, 5: 0, 100: 0}), True)
        self.assertEqual(is_zero({0: 0, 5: 1, 100: 0}), False)

    def test_cmp_sparsepolys(self):
        self.assertEqual(cmp_sparsepolys(self.zero, self.zero), True)
        self.assertEqual(cmp_sparsepolys(self.zero, {5: 0, 13: 0}), True)
        self.assertEqual(cmp_sparsepolys({1: 2, 3: 4}, {1: 2, 3: 4}), True)
        self.assertEqual(cmp_sparsepolys({1: 2, 3: 4}, {3: 4, 1: 2}), True)
        self.assertEqual(cmp_sparsepolys({1: 2, 3: 4}, {3: 4, 1: 3}), False)

    def test_pow_sparsepolys(self):
        self.assertEqual( pow_sparsepoly({1:2, 3: 4}, -1), {})
        self.assertEqual( pow_sparsepoly({1:2, 3: 4}, 0), {0: 1})
        self.assertEqual( pow_sparsepoly({1:2, 3: 4}, 1), {1:2, 3: 4})
        self.assertEqual( pow_sparsepoly({0:-1, 1: 1}, 2), {2:1, 1: -2, 0: 1})

    def test_combine_sparsepolys(self):
        self.assertEqual(combine_sparsepoly({}, {0:-1, 1: 1}), {})
        self.assertEqual(combine_sparsepoly({0: 12}, {0:-1, 1: 1}), {0: 12})
        self.assertEqual(combine_sparsepoly({0: 5, 1: 6, 2: 2}, {0:1, 1: 1}), {0: 13, 1: 10, 2: 2})
        self.assertEqual(combine_sparsepoly({0: 5, 1: 6, 2: 2}, {2: 1}), {0: 5, 2: 6, 4: 2})

    def test_diff_sparsepolys(self):
        self.assertEqual(diff_sparsepoly({}), {})
        self.assertEqual(diff_sparsepoly({0: 12}), {0: 0})
        self.assertEqual(diff_sparsepoly({0: 12, 1: 2, 3: 4}), {0: 2, 2: 12})

    def tearDown(self): pass


if __name__ == '__main__':
    unittest.main()  # uruchamia wszystkie testy

