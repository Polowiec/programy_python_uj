#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


def normalizujWielomian(wielomian):
    if len(wielomian) == 0:
        return [0]

    while len(wielomian) > 1:
        if wielomian[-1] == 0:
            del wielomian[-1]
        else:
            return wielomian
    return wielomian


def add_poly(poly1, poly2):  # poly1(x) + poly2(x)
    w = [0 for x in range(max(len(poly1), len(poly2)))]
    for x in range(len(poly1)):
        w[x] = poly1[x]
    for x in range(len(poly2)):
        w[x] += poly2[x]

    return normalizujWielomian(w)


def sub_poly(poly1, poly2):  # poly1(x) - poly2(x)
    w = [0 for x in range(max(len(poly1), len(poly2)))]
    for x in range(len(poly1)):
        w[x] = poly1[x]
    for x in range(len(poly2)):
        w[x] -= poly2[x]

    return normalizujWielomian(w)


def mul_poly(poly1, poly2):  # poly1(x) * poly2(x)
    w = [0 for x in range(len(poly1) * len(poly2))]
    for i in range(len(poly1)):
        for j in range(len(poly2)):
            w[i + j] += poly1[i] * poly2[j]
    return normalizujWielomian(w)


def is_zero(poly):  # bool, [0], [0,0], itp.
    return normalizujWielomian(poly) == [0]


def cmp_poly(poly1, poly2):  # bool, porównywanie
    return poly1 == poly2


def eval_poly(poly, x0):  # poly(x0), algorytm Hornera
    dl = len(poly)
    if dl == 0:
        return 0
    wynik = poly[-1]
    for x in range(dl - 1):
        wynik *= x0
        wynik += poly[-2 - x]
    return wynik


def combine_poly(poly1, poly2):  # poly1(poly2(x)), trudne!
    if len(poly1) * len(poly2) == 0:
        return []

    wynik = [0]
    for x in range(len(poly1)):
        wynik = add_poly(mul_poly(pow_poly(poly2, x), [poly1[x]]), wynik)

    return wynik


def pow_poly(poly, n):  # poly(x) ** n
    if n < 0:
        return []
    if n == 0:
        return [1]

    wynik = list(poly)
    for x in range(n - 1):
        wynik = mul_poly(wynik, poly)
    return normalizujWielomian(wynik)


def diff_poly(poly):  # pochodna wielomianu
    dl = len(poly)
    if dl == 0:
        return []
    if dl == 1:
        return [0]

    wynik = list()
    for x in range(dl - 1):
        wynik.append((x + 1) * poly[x + 1])
    return wynik


import unittest


class TestPolynomials(unittest.TestCase):
    def setUp(self):
        self.p1 = [0, 1]  # W(x) = x
        self.p2 = [0, 0, 1]  # W(x) = x*x
        self.zero = [0]

    def test_add_poly(self):
        self.assertEqual(add_poly(self.p1, self.p2), [0, 1, 1])
        self.assertEqual(add_poly([0], [0]), self.zero)
        self.assertEqual(add_poly([0], [0, 0]), self.zero)
        self.assertEqual(add_poly([0, 1], [0, -1]), self.zero)
        self.assertEqual(add_poly([1, 2], [3, 14, -2]), [4, 16, -2])

    def test_sub_poly(self):
        self.assertEqual(sub_poly(self.p1, self.p2), [0, 1, -1])
        self.assertEqual(sub_poly([0], [0]), self.zero)
        self.assertEqual(sub_poly([0], [0, 0]), self.zero)
        self.assertEqual(sub_poly([0, 1], [0, 1]), self.zero)
        self.assertEqual(sub_poly([1, 2], [3, 14, -2]), [-2, -12, 2])

    def test_mul_poly(self):
        self.assertEqual(mul_poly(self.p1, self.p1), self.p2)
        self.assertEqual(mul_poly(self.zero, self.p1), self.zero)
        self.assertEqual(mul_poly([2, 0, 4], [-2, 0, 4]), [-4, 0, 0, 0, 16])
        self.assertEqual(mul_poly([2, 0, 4], [8]), [16, 0, 32])

    def test_is_zero(self):
        self.assertEqual(is_zero(self.zero), True)
        self.assertEqual(is_zero([0, 0, 0]), True)
        self.assertEqual(is_zero([0, 0, 0, 0, 0]), True)
        self.assertEqual(is_zero([0, 1, 0, 0, 0]), False)
        self.assertEqual(is_zero([2, 1, 1, 2, 3]), False)

    def test_cmp_poly(self):
        self.assertEqual(cmp_poly([0], [0]), True)
        self.assertEqual(cmp_poly([2, 3, 4, 5], [2, 3, 4, 5]), True)
        self.assertEqual(cmp_poly([2, 3, 4, 5], [2, 3, 4, 5, 6]), False)

    def test_eval_poly(self):
        self.assertEqual(eval_poly([], 5), 0)
        self.assertEqual(eval_poly([1, 1, 1], 5), 31)
        self.assertEqual(eval_poly([1, 2, 1], 5), 36)
        self.assertEqual(eval_poly([1, -2, 1], 5), 16)
        self.assertEqual(eval_poly([9], 5), 9)
        self.assertEqual(eval_poly([1, 0, 0, 0, 1], 2), 17)

    def test_combine_poly(self):
        self.assertEqual(combine_poly([], [5]), [])
        self.assertEqual(combine_poly([10], [5, 3, -2, 8]), [10])
        self.assertEqual(combine_poly([5, 6, 2], [1, 1]), [13, 10, 2])
        self.assertEqual(combine_poly([5, 6, 2], [0, 0, 1]), [5, 0, 6, 0, 2])

    def test_pow_poly(self):
        self.assertEqual(pow_poly([1], 5), [1])
        self.assertEqual(pow_poly([1], -5), [])
        self.assertEqual(pow_poly([3, 4, 5], 0), [1])
        self.assertEqual(pow_poly([3, 1], 3), [27, 27, 9, 1])

    def test_diff_poly(self):
        self.assertEqual(diff_poly([1]), [0])
        self.assertEqual(diff_poly([]), [])
        self.assertEqual(diff_poly([5, 3, 2]), [3, 4])

    def tearDown(self): pass


if __name__ == '__main__':
    unittest.main()  # uruchamia wszystkie testy
