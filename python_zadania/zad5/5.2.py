#!/usr/bin/python
# -*- coding: utf-8 -*-
from zad5.fracs import is_positive

__author__ = 'Tomasz Polowiec'

from fracs import *

print(add_frac([1, 2], [1, 4]))
print(sub_frac([1, 2], [1, 4]))
print(mul_frac([3, 2], [1, 4]))
print(div_frac([3, 2], [1, 4]))
print(cmp_frac([3, 2], [1, 4]))
print(is_positive([-2, 3]))