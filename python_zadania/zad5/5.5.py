#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from sparsematrices import *

print(add_sparse({(1, 1): 4, (0, 1): 2}, {(1, 1): 2, (1, 0): 3}))
print(sub_sparse({(1, 1): 2, (0, 1): 2}, {(1, 1): 2, (1, 0): 3}))
print(mul_sparse({(0, 0): 1, (1, 1): 1}, {(1, 1): 2, (0, 1): 2}))
print(is_diagonal({(2, 2): 2, (4, 4): 3, (5, 1): 0}))
print(is_empty({(1, 1): 0, (300, 300): 0}))
