#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


class Node:
    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)  # bardzo ogólnie


def find_max(node):
    if node == None:
        return None
    max = node.data
    tmp = node.next
    while tmp:
        if tmp.data > max:
            max = tmp.data
        tmp = tmp.next
    return max


def find_min(node):
    if node == None:
        return None
    min = node.data
    tmp = node.next
    while tmp:
        if tmp.data < min:
            min = tmp.data
        tmp = tmp.next
    return min


head = None  # [], pusta lista
head = Node(3, head)  # [3]
head = Node(2, head)  # [2, 3]
head = Node(4, head)
head = Node(14, head)
head = Node(-2, head)
head = Node(10, head)

print("max", find_max(head))
print("min", find_min(head))
