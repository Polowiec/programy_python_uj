#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


class Node:
    def __init__(self, data=None, next=None, prev=None):
        self.data = data
        self.next = next
        self.prev = prev

    def __str__(self):
        return str(self.data)


class DoubleList:  # specjalna klasa na całą listę

    def __init__(self):
        self.length = 0
        self.head = None
        self.tail = None

    def is_empty(self):
        # return self.length == 0
        return self.head is None

    def count(self):
        return self.length

    def insert_head(self, data):
        if self.head:
            node = Node(data, next=self.head)
            self.head.prev = node  # stary head
            self.head = node  # nowy head
        else:  # pusta lista
            node = Node(data)
            self.head = node
            self.tail = node
        self.length = self.length + 1

    def insert_tail(self, data):
        if self.tail:
            node = Node(data, prev=self.tail)
            self.tail.next = node  # stary tail
            self.tail = node  # nowy tail
        else:  # pusta lista
            node = Node(data)
            self.head = node
            self.tail = node
        self.length = self.length + 1

    def remove_head(self):
        if self.head is None:
            raise ValueError("pusta lista")
        elif self.head is self.tail:
            data = self.head.data
            self.head = None
            self.tail = None
            self.length = 0
            return data
        else:
            data = self.head.data
            self.head = self.head.next
            self.head.prev = None
            self.length = self.length - 1
            return data

    def remove_tail(self):
        if self.head is None:
            raise ValueError("pusta lista")
        elif self.head is self.tail:
            data = self.tail.data
            self.head = None
            self.tail = None
            self.length = 0
            return data
        else:
            data = self.tail.data
            self.tail = self.tail.prev
            self.tail.next = None
            self.length = self.length - 1
            return data

    def remove_max(self):
        if self.length == 0:
            raise ValueError

        max_elem = self.head
        tmp = self.head.next
        while tmp:
            if tmp.data > max_elem.data:
                max_elem = tmp
            tmp = tmp.next

        if max_elem.prev != None:
            l = max_elem.prev
            l.next = max_elem.next
        else:
            self.head = max_elem.next

        if max_elem.next != None:
            p = max_elem.next
            p.prev = max_elem.prev
        else:
            self.tail = max_elem.prev

        wart = max_elem.data
        self.length -= 1
        del max_elem
        return wart

    def remove_min(self):
        if self.length == 0:
            raise ValueError

        min_elem = self.head
        tmp = self.head.next
        while tmp:
            if tmp.data < min_elem.data:
                min_elem = tmp
            tmp = tmp.next

        if min_elem.prev != None:
            l = min_elem.prev
            l.next = min_elem.next
        else:
            self.head = min_elem.next

        if min_elem.next != None:
            p = min_elem.next
            p.prev = min_elem.prev
        else:
            self.tail = min_elem.prev

        wart = min_elem.data
        self.length -= 1
        del min_elem
        return wart


alist = DoubleList()
alist.insert_head(11)
alist.insert_tail(-4)
alist.insert_tail(12)
alist.insert_tail(55)
alist.insert_tail(13)

print("Usunieto min", alist.remove_min())
print("Usunieto min", alist.remove_min())
print("Usunieto max", alist.remove_max())
print("Usunieto max", alist.remove_max())
print("Usunieto min", alist.remove_min())
print(alist.is_empty())
