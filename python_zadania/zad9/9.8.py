#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


class Node:
    def __init__(self, data=None, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

    def __str__(self):
        return str(self.data)


def max_bst(top):
    if top == None:
        raise ValueError
    tmp = top
    while tmp.right:
        tmp = tmp.right
    return tmp.data


def min_bst(top):
    if top == None:
        raise ValueError
    tmp = top
    while tmp.left:
        tmp = tmp.left
    return tmp.data


root = Node(5)
root.left = Node(2)
root.right = Node(10)
root.left.left = Node(-1)
root.left.right = Node(3)
root.right.left = Node(8)
root.right.right = Node(15)

print("max: ", max_bst(root))
print("min: ", min_bst(root))
