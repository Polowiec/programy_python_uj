#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from queue import Queue


class Node:
    def __init__(self, data=None, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

    def __str__(self):
        return str(self.data)


def count_leafs(top):
    if top == None:
        raise ValueError
    if top.left == None and top.right == None:
        return 1
    suma = 0
    if top.left != None:
        suma += count_leafs(top.left)
    if top.right != None:
        suma += count_leafs(top.right)
    return suma


def count_leafsIteracyjne(top):
    if top == None:
        raise ValueError
    kolejka = Queue()
    kolejka.put(top)
    liscie = 0
    while not kolejka.empty():
        elem = kolejka.get()
        if elem.right == None and elem.left == None:
            liscie += 1
        else:
            if elem.left != None:
                kolejka.put(elem.left)
            if elem.right != None:
                kolejka.put(elem.right)
    return liscie


def count_total(top):
    if top == None:
        return 0
    suma = top.data
    suma += count_total(top.left)
    suma += count_total(top.right)
    return suma


root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)

print("liscie: ", count_leafs(root))
print("liscie: ", count_leafsIteracyjne(root))
print("suma: ", count_total(root))
