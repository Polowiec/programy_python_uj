#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


class Node:
    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)  # bardzo ogólnie


def remove_head(node):
    if node == None:
        raise ValueError
    tmp = node
    wart = node.data
    node = node.next
    del tmp
    return (node, wart)


def remove_tail(node):
    if node == None:
        raise ValueError
    elif node.next == None:
        wart = node.data
        del node
        return (None, wart)
    else:
        poprz = node
        akt = node.next
        while akt.next:
            poprz, akt = akt, akt.next
        wart = akt.data
        del akt
        poprz.next = None
        return (node, wart)


head = None  # [], pusta lista
head = Node(3, head)  # [3]
head = Node(2, head)  # [2, 3]
head = Node(4, head)
while head:
    head, data = remove_head(head)
    print("usuwam", data)

print()

head = Node(3, head)  # [3]
head = Node(2, head)  # [2, 3]
head = Node(4, head)
while head:
    head, data = remove_tail(head)
    print("usuwam", data)
