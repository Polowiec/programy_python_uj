#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


class Node:
    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)  # bardzo ogólnie


class SortedList:
    def __init__(self):
        self.lenght = 0
        self.head = None

    def is_empty(self):
        return self.lenght == 0

    def count(self):
        return self.lenght

    def insert(self, data):
        if self.head == None:
            self.head = Node(data)
        elif self.head.data >= data:
            self.head = Node(data, self.head)
        else:
            tmp = self.head
            while tmp.next:
                if tmp.next.data >= data:
                    break
                tmp = tmp.next
            tmp.next = Node(data, tmp.next)
        self.lenght += 1

    def remove(self):  # zwraca element największy
        if self.lenght == 0:
            raise ValueError
        elif self.lenght == 1:
            wart = self.head.data
            del self.head
            self.head = None
            self.lenght = 0
            return wart
        else:
            tmp = self.head
            while tmp.next.next:
                tmp = tmp.next
            tmp2 = tmp.next
            tmp.next = None
            wart = tmp2.data
            del tmp2
            self.lenght -= 1
            return wart


lista = SortedList()
lista.insert(3)
lista.insert(13)
lista.insert(5)
lista.insert(-3)
lista.insert(0)
lista.insert(7)
lista.insert(1)
while not lista.is_empty():
    print("delete: ", lista.remove())
