#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


class Node:
    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)  # bardzo ogólnie


def merge(node1, node2):
    if node1 == None:
        return node2

    tmp = node1
    while tmp.next:
        tmp = tmp.next
    tmp.next = node2
    return node1


head1 = None  # [], pusta lista
head1 = Node(3, head1)  # [3]
head1 = Node(2, head1)  # [2, 3]
head1 = Node(4, head1)

head2 = None
head2 = Node(6, head2)
head2 = Node(9, head2)
head2 = Node(8, head2)

head = merge(head1, head2)
head1 = None
head2 = None

tmp = head
while tmp:
    print(tmp.data)
    tmp = tmp.next
