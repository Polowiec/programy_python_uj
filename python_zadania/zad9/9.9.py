#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


class Node:
    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)  # bardzo ogolnie


def liczba_wezlow(node):
    if node == None:
        raise ValueError
    ilosc = 1
    tmp = node
    while tmp.next != node:
        ilosc += 1
        tmp = tmp.next
    return ilosc


N = 5
head = Node(1)
head.next = head  # lista cykliczna
# Wstawiamy następne liczby za head.
node = head
for i in range(2, N + 1):
    node.next = Node(i, head)
    node = node.next

print("ilosc elementow: ", liczba_wezlow(head))
print("ilosc elementow: ", liczba_wezlow(head.next.next))
