#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


# Problem drogi skoczka na kwadratowej szachownicy o boku N.
# Współrzędne planszy x i y mają zakres od 0 do N-1.

def rysuj():
    for i in range(N):
        wiersz = ""
        for j in range(N):
            wiersz += "%3s " % plansza[i, j]
        print(wiersz)


def dopuszczalny(x, y):
    return 0 <= x < N and 0 <= y < N and plansza[x, y] == 0


def zapisz(krok, x, y):
    plansza[x, y] = krok  # zapis ruchu


def wymaz(x, y):
    plansza[x, y] = 0


def probuj(krok, x, y):
    # krok - nr kolejnego kroku do zrobienia
    # x, y - pozycja startowa skoczka
    # Funkcja zwraca bool True/False (czy udany ruch).
    udany = False
    kandydat = 0  # numery od 0 do RUCHY_SKOCZKA-1
    while (not udany) and (kandydat < RUCHY_SKOCZKA):
        u = x + delta_x[kandydat]  # wybieramy kandydata
        v = y + delta_y[kandydat]
        if dopuszczalny(u, v):
            zapisz(krok, u, v)
            if krok < N * N:  # warynek końca rekurencji
                udany = probuj(krok + 1, u, v)
                if not udany:
                    wymaz(u, v)
            else:
                udany = True
        kandydat = kandydat + 1
    return udany


N = 0  # bok szachownicy
RUCHY_SKOCZKA = 8

# Inicjalizacja pustej planszy.
plansza = {}

delta_x = [2, 1, -1, -2, -2, -1, 1, 2]  # różnica współrzędnej x
delta_y = [1, 2, 2, 1, -1, -2, -2, -1]  # różnica współrzędnej y


def problemSkoczka(rozmiar, startX, startY):
    global plansza
    global N
    if rozmiar < 1 or startX < 0 or startY < 0 or startX >= rozmiar or startY >= rozmiar:
        raise ValueError
    N = rozmiar
    plansza = {}
    for i in range(N):
        for j in range(N):
            plansza[i, j] = 0

    zapisz(1, startX, startY)

    print("Rozmiar N= %s, start: (%s, %s)" % (rozmiar, startX, startY))
    if probuj(2, startX, startY):
        print("Mamy rozwiązanie")
        rysuj()
    else:
        print("Nie istnieje rozwiązanie")
    print()
