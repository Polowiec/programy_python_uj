#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

plansza = []
historia = []
N = 0


def probuj(poz):
    if (poz == N * N):
        if plansza in historia:
            return False
        else:
            historia.append(plansza)
            return True
    x = int(poz / N)
    y = int(poz % N)

    for k in range(N):
        if sprawdz(poz, k + 1):
            plansza[x * N + y] = k + 1
            zwrot = probuj(poz + 1)
            if zwrot:
                return True
    plansza[x * N + y] = 0
    return False


def sprawdz(poz, wart):
    x = int(poz / N)
    y = int(poz % N)

    for k in range(N):
        if wart == plansza[x * N + k] or wart == plansza[k * N + y]:
            return False
    return True


def rysuj():
    for x in range(N):
        wiersz = ""
        for y in range(N):
            wiersz += str(plansza[x * N + y]) + "  "
        print(wiersz)


def znajdzKwadraty(rozmiar):
    if rozmiar < 1:
        raise ValueError
    global plansza
    global N
    global historia
    N = rozmiar
    historia = []
    nr = 0
    plansza = [0] * (rozmiar * rozmiar)

    while probuj(0):
        print("Kwadrat: %s" % nr)
        rysuj()
        nr += 1
        plansza = [0] * (rozmiar * rozmiar)
