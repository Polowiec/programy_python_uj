#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from math import sqrt

plansza = []
historia = []
N = 0
bok = 0


def probuj(poz):
    if (poz == N * N):
        if plansza in historia:
            return False
        else:
            historia.append(plansza)
            return True
    x = int(poz / N)
    y = int(poz % N)

    for k in range(N):
        if sprawdz(poz, k + 1):
            plansza[x * N + y] = k + 1
            zwrot = probuj(poz + 1)
            if zwrot:
                return True
    plansza[x * N + y] = 0
    return False


def sprawdz(poz, wart):
    x = int(poz / N)
    y = int(poz % N)

    for k in range(N):
        if wart == plansza[x * N + k] or wart == plansza[k * N + y]:
            return False
    return sprawdzKwadrat(poz, wart)


def sprawdzKwadrat(poz, wart):
    x = int(poz / N)
    y = int(poz % N)
    bX = int(x / bok)
    bY = int(y / bok)

    for i in range(bok):
        for j in range(bok):
            if plansza[(bX * bok + i) * N + bY * bok + j] == wart:
                return False
    return True


def rysuj():
    poziomaBelka = "+"
    b = ""
    for x in range(2 * bok + 1):
        b += "-"
    b += "+"
    for x in range(bok):
        poziomaBelka += b

    for x in range(N):
        if x % bok == 0:
            print(poziomaBelka)
        wiersz = ""
        for y in range(N):
            if y % bok == 0:
                wiersz += "| "
            wiersz += str(plansza[x * N + y]) + " "
        wiersz += "|"
        print(wiersz)
    print(poziomaBelka)


def znajdzSudoku(rozmiar):
    if rozmiar < 1:
        raise ValueError
    pierwiastek = sqrt(rozmiar)
    global bok
    bok = int(pierwiastek)
    if pierwiastek != int(pierwiastek):
        raise ValueError

    global plansza
    global N
    global historia
    N = rozmiar
    historia = []
    nr = 0
    plansza = [0] * (rozmiar * rozmiar)

    while probuj(0):
        print("Sudoku: %s" % nr)
        rysuj()
        nr += 1
        plansza = [0] * (rozmiar * rozmiar)
