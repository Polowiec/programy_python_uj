#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

plansza = []
historia = []
N = 0
deska = 0


def probuj(pozycja):
    if pozycja == N * N:
        if plansza in historia:
            return False
        else:
            historia.append(plansza)
            return True
    x = int(pozycja % N)
    y = int(pozycja / N)

    if plansza[y * N + x] != 0:
        return probuj(pozycja + 1)


    # najpierw dodaj pion
    if probujDodac(pozycja, 1):
        zwrot = probuj(pozycja + 1)
        if zwrot:
            return True
        else:
            cofnij(pozycja, 1)
    # jak sie nie udalo to poziomo
    if probujDodac(pozycja, 2):
        zwrot = probuj(pozycja + 1)
        if zwrot:
            return True
        else:
            cofnij(pozycja, 2)
    return False


def probujDodac(pozycja, kierunek):
    # kierunek = 1 pion
    # kierunek = 2 poziom
    x = int(pozycja % N)
    y = int(pozycja / N)

    if kierunek == 1:
        # pion
        if y + deska - 1 >= N:
            return False
        for k in range(deska):
            if plansza[(y + k) * N + x] != 0:
                return False
        for k in range(deska):
            plansza[(y + k) * N + x] = 1
        return True
    else:
        # poziom
        if x + deska - 1 >= N:
            return False
        for k in range(deska):
            if plansza[y * N + x + k] != 0:
                return False
        for k in range(deska):
            plansza[y * N + x + k] = 2
        return True


def cofnij(pozycja, kierunek):
    global plansza
    x = int(pozycja % N)
    y = int(pozycja / N)
    if kierunek == 1:
        # pion
        for k in range(deska):
            plansza[(y + k) * N + x] = 0
    else:
        # poziom
        for k in range(deska):
            plansza[y * N + x + k] = 0


def rysuj():
    for y in range(N):
        wiersz = ""
        for x in range(N):
            wiersz += str(plansza[y * N + x]) + " "
        print(wiersz)


def szukajUlozenParkietu(rozmiarParkietu, dlugosDeski):
    if rozmiarParkietu < 1 or dlugosDeski < 1:
        raise ValueError
    global plansza
    global N
    global deska
    global historia
    N = rozmiarParkietu
    deska = dlugosDeski
    plansza = [0] * (N * N)
    historia = []

    nr = 0
    while probuj(0):
        print("Rozwiazanie %s" % nr)
        rysuj()
        print()
        nr += 1
        plansza = [0] * (N * N)

    print()
    print("Lacznie %s rozwiazan" % nr)
