#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

line = "Zbudować napis stworzony z pierwszych " \
       "znaków wyrazów z wiersza line. Zbudować napis " \
       "stworzony z ostatnich znaków wyrazów z wiersza line."


def poczatkoweLitery(napis):
    tablica = napis.split();
    nowy_napis = ""
    for x in range(0, len(tablica)):
        nowy_napis += tablica[x][0]
    return nowy_napis;


def koncoweLitery(napis):
    tablica = napis.split();
    nowy_napis = ""
    for x in range(0, len(tablica)):
        pom = tablica[x]
        nowy_napis += pom[len(pom) - 1]
    return nowy_napis;


print(poczatkoweLitery(line))
print(koncoweLitery(line))
