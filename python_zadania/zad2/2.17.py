#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

line = "Posortować wyrazy z napisu line raz alfabetycznie, " \
       "a raz pod względem długości. Wskazówka: funkcja wbudowana sorted()."


def sortowanieAlfabetyczne(napis):
    tablica = napis.split()
    return sorted(tablica, key=str.lower)


def sortowanieDlugosc(napis):
    tablica = napis.split()
    return sorted(tablica, key=len)


print(sortowanieAlfabetyczne(line))
print(sortowanieDlugosc(line))
