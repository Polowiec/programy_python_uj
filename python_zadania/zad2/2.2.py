#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

print( 5 == 5 ), print( 4 > 5 )
print( type(True) ), print( type(False) )
print( type(1) ), print( type(0) )
x = 5
print( x == 5 and 3 )
print( x == 4 and 3 )
print( 3 and x == 5 )
print( 3 and x == 4 )
print( 1 < x < 9 )
print( isinstance(True, int) )
print( isinstance(True, bool) )