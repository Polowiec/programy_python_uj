#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

line = "Znaleźć łączną długość wyrazów w napisie line."

def lacznaDlugosWyrazow(napis):
    tablica= napis.split()
    return len(''.join(tablica))

print(lacznaDlugosWyrazow(line))