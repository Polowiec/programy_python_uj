#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


def wypiszNapisZBelkami(napis):
    dlugos = len(napis);

    if dlugos <= 1:
        print(napis)
    else:
        nowy_napis = "";
        for x in range(0, dlugos - 1):
            nowy_napis+= napis[x] + "_"
        nowy_napis+= napis[dlugos - 1]
        print(nowy_napis)


wypiszNapisZBelkami("przykladowy napis")
