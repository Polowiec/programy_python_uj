#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

import math

i = -4  # int
l = 123456789000  # long     0L nie dziala od pytona 3.5
f = 2.4  # float
f2 = 2.43e+23
c = 2 + .3j, 8.9 - 7J  # complex
z = 5 + 1j  # typ complex

print(str(i)), print(repr(i))
print(str(l)), print(repr(l))
print(str(f)), print(repr(f))
print(str(f2)), print(repr(f2))
print(str(c)), print(repr(c))
print(str(z)), print(repr(z))

print("")
# Konwersja typów niejawna.
print(1 + 1.2), print(34 / 60.)  # int + float = float
print(1.2 + 3J)  # float + complex = complex

print("")
print(int("32")), print(int(2.345)), print(int(-3.456))
print(int("111", 2)), print(int("20", 16))  # liczba po stringu oznacza baze
print(float("3.14159")), print(float(234))
#print(float(1.0+2.j))                # TypeError, nie ma jednoznacznej konwersji
print(str(1234))
#long(43)
print(complex(5)), print(complex(2,3))                # (5+0J), (2+3J)

print("")
print( pow(2, 5))
print( pow(2,5, 10)) #2^5 % 10
print( abs( -6))
print( round(3.1415))
print( round(3.1415, 3))
print( min(1, 2, 3, 4, 5))
print( max(1, 2, 3, 4, 5))
liczby= [1, 2, 3, 4, 5]
print( sum(liczby))

print("")
print(5 / 2)
print(5 % 2)
print(5. / 2.)

print("")
x= 2.3
print( round(x))
print( int(x+0.5))
print( math.trunc(x))

print("")
#print( 3/0 )