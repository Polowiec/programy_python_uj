#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

L = [3, "xyz", [10, 20]]
print(len(L))
print(L[1]), print(L[1][0]), print(L[2]), print(L[2][1])
M = L
print(M)
L[1] = 5
print(L[1])
print(M[1])

print()
L = ["a", "a", "a", "b", "b"]
print( L.count("a"))
L.append("c")
print(L)
L.insert(2, "x")
print(L)
L.remove("a")
print(L)


print()
L.pop(2)
print(L)
L.reverse()
print(L)
print(L.index("x"))

print()
L.extend("napis")
print(L)
L.sort()
print(L)

print()
alist = ["auto", "Ala", "bocian", "Barbara"]
alist.sort(key=len)
print(alist)

def cmp(a, b):
    return (a > b) - (a < b)

#alist.sort(cmp=lambda x,y: cmp(x.lower(), y.lower()))
#print( alist )

print()
print([x for x in "qwerty"]       )            # ['q', 'w', 'e', 'r', 't', 'y']
print([x * x for x in range(6)]  )             # [0, 1, 4, 9, 16, 25]
print([3 * x for x in range(6) if x < 2] )  # [0, 3, 6, 9]
print([2 ** x for x in range(5)]   )           # [1, 2, 4, 8, 16]
print([[x, x * x] for x in range(6)]   )       # lista list
print([(x, x * x * x) for x in range(6)]  )    # przy krotkach wymagane nawiasy
print([x + y for x in range(3) for y in range(4)])
print([(x, y) for x in range(3) for y in range(4)])
print([chr(x) for x in range(97, 110)])

L1= [1, 2, 3]
L2= list(L1)
L2[1]= 7
print(L2[1])
print(L1[1])
print(id(L1))
print(id(L2))