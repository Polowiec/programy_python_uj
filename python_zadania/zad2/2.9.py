#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


def kopiujBezKomentarzy(inputName, outputName):
    inpFile = open(inputName, "r")
    outFile = open(outputName, "w")
    while True:
        linia = inpFile.readline()
        if linia == "":
            break           # koniec pliku
        if linia.lstrip()[0] != '#':
            outFile.write(linia)
    inpFile.close()
    outFile.close()


kopiujBezKomentarzy("source.txt", "copy.txt")