#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

line = "Jakiś tam napis i skrót GvR który ma być zamieniony na coś innego. GvR."


def zamienGvR(napis):
    return napis.replace("GvR", "Guido van Rossum")


print(zamienGvR(line))
