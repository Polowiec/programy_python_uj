#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


def liczbaZer(liczba):
    napis = str(liczba)
    licznik = 0
    for x in range(0, len(napis)):
        if napis[x] == '0':
            licznik += 1
    return licznik


print(liczbaZer(123001230123))
print(liczbaZer(900000000000))
