#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

print(len("napis"))
print(len(str(2 ** 10000)))

print("abc")
print('abc')
S = "ab"    'cd'
print(S)

print("ab'cd")
print('ab"cd')
S = "a\tb\nc\"d"
print(S)
print(len(S))

S = """jeden
dwa
trzy"""
print(S)

print("")
S = "abradakadabra"
print(S[2])
print(S[-3])
print(S[3:5])
print(S[3:])
print(S[:4])

print("")
L = ["a", "b", "c"]
print(L[0] + L[1] + L[2])
print(L[0] + "=" + L[1] + "=" + L[2])
print("".join(L))
print("=".join(L))
print(S.join(["1", "2", "3"]))

print()
print(S.find("kad"))
print(S.replace("a", "c"))
print(S.replace("a", "c", 2))
print("ooo!tekst!ooo".strip("o"))
print("AbCd".lower())
print("AbCd".upper())
print( "aaabc".zfill(8))
