#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

line = "Znaleźć: (a) najdłuższy wyraz, (b) długość najdłuższego wyrazu w napisie line."


def najdluzszyWyraz(napis):
    tablica = napis.split()
    najdluzszy = tablica[0];
    maxDlugosc = len(najdluzszy)

    for x in range(1, len(tablica)):
        if len(tablica[x]) > maxDlugosc:
            najdluzszy = tablica[x];
            maxDlugosc = len(najdluzszy)
    return najdluzszy


def dlugoscNajdluzszegoSlowa(napis):
    tablica = napis.split()
    maxDlugosc = len(tablica[0])

    for x in range(1, len(tablica)):
        if len(tablica[x]) > maxDlugosc:
            maxDlugosc = len(tablica[x])
    return maxDlugosc


print(najdluzszyWyraz(najdluzszyWyraz(line)))
print(dlugoscNajdluzszegoSlowa(line))
