#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

def napisZLiczbTrzycyfrowych(lista):
    napis= ""
    for x in range( 0, len(lista)):
        napis+= str(lista[x]).zfill(3)
    return napis


liczby= [23, 554, 3, 34, 33, 1, 0, 2, 999]
print(napisZLiczbTrzycyfrowych(liczby))