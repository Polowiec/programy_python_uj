#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

line = """Mamy dany napis wielowierszowy line.
Podać sposób obliczenia liczby
wyrazów w napisie."""


def liczbaWyrazow(napis):
    tablica = napis.split()
    return len(tablica)


print(liczbaWyrazow(line))