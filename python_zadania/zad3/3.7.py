#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


class Time:
    def __init__(self, seconds=0):
        self.s = seconds

    def __str__(self):
        return "%s sec" % self.s

    def __repr__(self):
        return "Time(%s)" % self.s


time1 = Time(12)
time2 = Time(3456)
print(time1, time2)  # Python wywołuje str()
print([time1, time2])  # Python wywołuje repr()
print()


class Time1:
    def __init__(self, seconds=0):
        self.s = seconds

    # def __str__(self):
    #    return "%s sec" % self.s
    def __repr__(self):
        return "Time(%s)" % self.s


time1 = Time1(12)
time2 = Time1(3456)
print(time1, time2)  # Python wywołuje str()
print([time1, time2])  # Python wywołuje repr()
print()


class Time2:
    def __init__(self, seconds=0):
        self.s = seconds

    def __str__(self):
        return "%s sec" % self.s
    # def __repr__(self):
    #    return "Time(%s)" % self.s


time1 = Time2(12)
time2 = Time2(3456)
print(time1, time2)  # Python wywołuje str()
print([time1, time2])  # Python wywołuje repr()
