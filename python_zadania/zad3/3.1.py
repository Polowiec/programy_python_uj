#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

#fragment 1
x = 2 ; y = 3 ;
if (x > y):
    result = x;
else:
    result = y;
#kod poprawny skladniowo
print(result)



#fragment 2
#   for i in "qwerty": if ord(i) < 100: print i
#kod niepoprawny
#musza byc wciecia dla bloku fora i ifa
for i in "qwerty":
    if ord(i) < 100:
        print(i)



#fragment 3
#for i in "axby": print ord(i) if ord(i) < 100 else i
#poprawne wyrazenie, wyrazenie w princie jest wyrazeniem trojargumentowym i jest ok
for i in "axby": print( ord(i) if ord(i) < 100 else i ) #dla pythona 3.5
#dla prostych wyrazen python radzi sobie z zapisem w jednej linijce, ale lepiej tego unikac