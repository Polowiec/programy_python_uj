#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

# fragment 1
# L = L.sort()
# metoda sort sortuje obiekty na rzecz listy L i nic nie zwraca,
# jeśli L będzie obiektem ktory nie ma metody sort np. zwykły int to poleci błąd
L = [1, 2, 3]
L.sort()  # bedzie ok



# fragment2
# x, y = 1, 2, 3
# wiecej wartosci nic zmiennych
x, y, z = 1, 2, 3  # juz jest ok
print(x)
print(y)
print(z)



# fragment3
X = 1, 2, 3  # ; X[1] = 4
print()
print(X[1])
# X jest krotka, elementy krotki sa read-only, zatem mozna je odczytac ale zmienic pojedynczego elemntu juz nie



# fragment4
X = [1, 2, 3]  # ; X[3] = 4
# element o indexie 3 nie istnieje, aby go dodac nalezy wywolac metode append
X.append(4)
print(X)



# fragment5
X = "abc";  # X.append("d")
# X jest stringiem a nie lista, string nie ma metody append
# nalezy albo przerobić string na liste, albo uzyc metod ze stringa
X += "d"
print(X)



# fragment6
# map(pow, range(8))
# funkcja pow wymaga 2 argumentow, zatem w map nalezy dodac jeszcze jeden iterowalny argument
X = map(pow, range(8), [2 for l in range(8)])
print()
for elem in X:
    print(elem)
