#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


def stworzMiarke(dlugosc):
    if dlugosc <= 0:
        return ""

    miarka = "|"
    for x in range(dlugosc):
        miarka += "....|"
    miarka += "\n"

    miarka += "0"
    for x in range(dlugosc):
        miarka += str(x + 1).rjust(5)

    return miarka


print(stworzMiarke(15))
