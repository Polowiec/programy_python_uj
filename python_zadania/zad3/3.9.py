#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


def sumyZSekwencji(sekwencja):
    lista_sum = list()
    for elem in sekwencja:
        lista_sum.append(sum(elem))

    return lista_sum


sekw = [[], [4], (1, 2), [3, 4], (5, 6, 7)]
print(sumyZSekwencji(sekw))
