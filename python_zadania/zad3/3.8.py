#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


def elementyWspolne(lista1, lista2):
    czesc_wspolna = set(lista1) & set(lista2)
    return list(czesc_wspolna)


def wszystkieElementy(lista1, lista2):
    suma = set(lista1) | set(lista2)
    return list(suma)


A = [1, 3, 5, 7, 9, 12]
B = [0, 3, 9, 12, 15]

print(elementyWspolne(A, B))
print(wszystkieElementy(A, B))
