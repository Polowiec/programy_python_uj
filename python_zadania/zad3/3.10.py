#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

# I, V, X, L, C, D, M
slownik = {'I': 1, 'V': 5}  # 1szy sposob dodwania
# slownik= dict([('I', 1), ('V', 5)])      #zamiennie z linijka wyzej, zrobienie slownika z krotek
slownik['X'] = 10  # drugi sposob
slownik['L'] = 50
slownik['C'] = 100
slownik['D'] = 500
slownik['M'] = 1000


def roman2int(roman):
    wartosc = 0
    poprz_w = 0
    akt_w = 0
    for s in roman:
        akt_w = slownik[s]
        if (poprz_w < akt_w):
            wartosc -= poprz_w
        else:
            wartosc += poprz_w
        poprz_w = akt_w
    wartosc += akt_w
    return wartosc


print(roman2int("XXX"))
print(roman2int("XXIX"))
print(roman2int("MMDCLXVI"))
print(roman2int("IV"))
print(roman2int("VI"))
print(roman2int("XIII"))
print(roman2int("CDLXXXIX"))
print(roman2int("I"))
print(roman2int("II"))
print(roman2int(""))
