#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

while True:
    slowo = input("Podaj liczbe\n")  # w pythonie 3.x raw_input zostalo zamienione na input
    try:
        liczba = float(slowo)
        print(pow(liczba, 3))
    except:
        if slowo == "stop":
            break
        else:
            print("Bledna liczba")
