#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'


class Poly:
    """Klasa reprezentująca wielomiany."""

    def normalizujWielomian(self):
        while self.size > 1:
            if self.a[-1] == 0:
                del self.a[-1]
                self.size -= 1
            else:
                break

        return self

    # wg Sedgewicka - tworzymy wielomian c*x^n
    def __init__(self, c=0, n=0):
        if n < 0:
            raise ValueError
        self.size = n + 1  # rozmiar tablicy
        self.a = self.size * [0]
        self.a[self.size - 1] = c
        self.normalizujWielomian()

    def __str__(self):
        return str(self.a)

    def __add__(self, other):  # poly1+poly2
        if isinstance(other, (int, float)):
            other = Poly(other, 0)

        p = Poly()
        max_len = max(self.size, other.size)
        p.size = max_len
        p.a = p.size * [0]
        for x in range(self.size):
            p.a[x] += self.a[x]
        for x in range(other.size):
            p.a[x] += other.a[x]
        p.normalizujWielomian()
        return p

    __radd__ = __add__

    def __sub__(self, other):  # poly1-poly2
        if isinstance(other, (int, float)):
            other = Poly(other, 0)

        p = Poly()
        max_len = max(self.size, other.size)
        p.size = max_len
        p.a = p.size * [0]
        for x in range(self.size):
            p.a[x] += self.a[x]
        for x in range(other.size):
            p.a[x] -= other.a[x]
        p.normalizujWielomian()
        return p

    def __rsub__(self, other):
        other = Poly(other, 0)
        return other - self

    def __mul__(self, other):  # poly1*poly2
        if isinstance(other, (int, float)):
            other = Poly(other, 0)

        p = Poly()
        max_len = self.size + other.size
        p.size = max_len
        p.a = p.size * [0]
        for i in range(self.size):
            for j in range(other.size):
                p.a[i + j] += self.a[i] * other.a[j]
        p.normalizujWielomian()
        return p

    __rmul__ = __mul__

    def __pos__(self):  # +poly1
        return self

    def __neg__(self):  # -poly1
        p = Poly()
        p.size = self.size
        p.a = list(self.a)
        for x in range(p.size):
            p.a[x] *= -1
        return p

    def __eq__(self, other):  # obsługa poly1 == poly2
        return self.a == other.a

    def __ne__(self, other):  # obsługa poly1 != poly2
        return not self == other

    def eval(self, x):  # schemat Hornera
        dl = len(self.a)
        wynik = self.a[-1]
        for i in range(dl - 1):
            wynik *= x
            wynik += self.a[-2 - i]
        return wynik

    def combine(self, other):  # złożenie poly1(poly2(x))
        wynik = Poly(0, 0)
        for x in range(self.size):
            wynik = wynik + ((other ** x) * Poly(self.a[x], 0))
        return wynik

    def __pow__(self, n):  # poly(x)**n lub pow(poly(x),n)
        if n < 0:
            raise ValueError
        if n == 0:
            return Poly(1, 0)

        wynik = Poly(1, 0)
        for x in range(n):
            wynik = wynik * self
        return wynik

    def diff(self):  # różniczkowanie
        if self.size == 1:
            return Poly(0, 0)

        p = Poly()
        p.size = self.size - 1
        wynik = list()
        for x in range(self.size - 1):
            wynik.append((x + 1) * self.a[x + 1])
        p.a = wynik
        p.normalizujWielomian()
        return p

    def integrate(self):  # całkowanie
        p = Poly()
        p.size = self.size + 1
        wynik = [0]
        for x in range(self.size):
            wynik.append(1.0 / (x + 1) * self.a[x])
        p.a = wynik
        p.normalizujWielomian()
        return p

    def is_zero(self):  # bool, True dla [0], [0, 0],...
        return self.a == [0]

    def __len__(self):
        return self.size

    def __getitem__(self, item):
        if item < 0 or item >= self.size:
            raise ValueError
        return self.a[item]

    def __setitem__(self, key, value):
        if key < 0:
            raise ValueError
        if key >= self.size:
            for x in range(key - self.size):
                self.a.append(0)
            self.a.append(value)
            self.size = key + 1
            self.normalizujWielomian()
        else:
            self.a[key] = value
            self.normalizujWielomian()

    def __call__(self, x):
        if isinstance(x, (int, float)):
            return self.eval(x)
        elif isinstance(x, Poly):
            return self.combine(x)


# Kod testujący moduł.

import unittest


class TestPoly(unittest.TestCase):
    def setUp(self):
        self.p1 = Poly(5, 2)
        self.p2 = Poly(7, 2)
        self.p3 = Poly(3, 3)

    def test_raise(self):
        self.assertRaises(ValueError, lambda: Poly(3, -5))

    def test_print(self):
        self.assertEqual(str(self.p1), "[0, 0, 5]")
        self.assertEqual(str(self.p2), "[0, 0, 7]")
        self.assertEqual(str(Poly(0, 10)), "[0]")

    def test_add(self):
        self.assertEqual(self.p1 + self.p2, Poly(12, 2))
        self.assertEqual(str(self.p1 + self.p3), "[0, 0, 5, 3]")
        self.assertEqual(str(self.p1 + Poly(-5, 2)), "[0]")
        self.assertEqual(str(self.p1 + self.p2 + self.p3), "[0, 0, 12, 3]")

    def test_add_float(self):
        self.assertEqual(self.p1 + 0, self.p1)
        self.assertEqual(Poly(7, 0) + 10, Poly(17, 0))
        self.assertEqual(Poly(7, 3) + 10, Poly(7, 3) + Poly(10, 0))
        self.assertEqual(10 + Poly(7, 0), Poly(17, 0))
        self.assertEqual(-7 + Poly(7, 0), Poly(0, 0))

    def test_sub(self):
        self.assertEqual(self.p1 - self.p1, Poly(0, 0))
        self.assertEqual(self.p1 - self.p2, Poly(-2, 2))
        self.assertEqual(self.p1 - Poly(-1, 2), Poly(6, 2))
        self.assertEqual(str(self.p3 - self.p2), "[0, 0, -7, 3]")

    def test_sub_float(self):
        self.assertEqual(self.p3 - 0, self.p3)
        self.assertEqual(0 - self.p3, -self.p3)
        self.assertEqual(Poly(7, 3) - 10, Poly(7, 3) + Poly(-10, 0))
        self.assertEqual(10 - Poly(7, 0), Poly(3, 0))

    def test_mul(self):
        self.assertEqual(self.p1 * self.p2, Poly(35, 4))
        self.assertEqual(self.p1 * Poly(0, 0), Poly(0, 0))
        self.assertEqual((Poly(1, 1) + Poly(1, 0)) * (Poly(1, 1) + Poly(-1, 0)), Poly(1, 2) + Poly(-1, 0))

    def test_mul_float(self):
        self.assertEqual(0 * self.p2, Poly(0, 0))
        self.assertEqual(1 * self.p2, self.p2)
        self.assertEqual(2 * Poly(4, 3), Poly(8, 3))
        self.assertEqual(self.p2 * 0, Poly(0, 0))
        self.assertEqual(self.p2 * 1, self.p2)
        self.assertEqual(Poly(4, 3) * 2, Poly(8, 3))

    def test_pos(self):
        self.assertEqual(+self.p1, self.p1)
        self.assertEqual(+self.p2, self.p2)

    def test_neg(self):
        self.assertEqual(-self.p1, Poly(-5, 2))
        self.assertEqual(str(-(self.p1 + self.p3)), "[0, 0, -5, -3]")

    def test_compare(self):
        self.assertTrue(self.p1 == self.p1)
        self.assertTrue(Poly(0, 9) == Poly(0, 2))
        self.assertFalse(self.p2 == self.p3)
        self.assertTrue(self.p2 != self.p3)

    def test_eval(self):
        self.assertEqual(self.p1.eval(0), 0)
        self.assertEqual(self.p1.eval(3), 45)
        self.assertEqual((self.p1 + self.p3).eval(1), 8)

    def test_pow(self):
        self.assertEqual(self.p1 ** 0, Poly(1, 0))
        self.assertEqual(self.p1 ** 1, self.p1)
        self.assertEqual(self.p1 ** 2, Poly(25, 4))
        self.assertRaises(ValueError, lambda: self.p1 ** -2)

    def test_combine(self):
        self.assertEqual(self.p1.combine(Poly(0, 0)), Poly(0, 0))
        self.assertEqual(self.p1.combine(Poly(1, 0)), Poly(5, 0))
        self.assertEqual(self.p1.combine(Poly(1, 2)), Poly(5, 4))

    def test_diff(self):
        self.assertEqual(Poly(0, 0).diff(), Poly(0, 0))
        self.assertEqual(Poly(10, 0).diff(), Poly(0, 0))
        self.assertEqual(self.p1.diff(), Poly(10, 1))
        self.assertEqual((self.p1 + self.p3).diff(), Poly(10, 1) + Poly(9, 2))

    def test_integrate(self):
        self.assertEqual(Poly(0, 0).integrate(), Poly(0, 0))
        self.assertEqual(Poly(4, 0).integrate(), Poly(4, 1))
        self.assertEqual(Poly(4, 2).integrate(), Poly(4.0 / 3, 3))
        self.assertEqual((Poly(4, 2) + Poly(6, 5)).integrate(), Poly(4.0 / 3, 3) + Poly(1, 6))

    def test_zero(self):
        self.assertTrue(Poly(0, 0).is_zero())
        self.assertTrue(Poly(0, 10).is_zero())
        self.assertFalse(Poly(1, 10).is_zero())

    def test_len(self):
        self.assertEqual(len(self.p1), 3)
        self.assertEqual(len(Poly(1, 0)), 1)
        self.assertEqual(len(Poly(0, 10)), 1)
        self.assertEqual(len(Poly(5, 4)), 5)

    def test_getitem(self):
        self.assertEqual(Poly(2, 0)[0], 2)
        self.assertEqual(Poly(2, 3)[0], 0)
        self.assertEqual(Poly(2, 3)[3], 2)
        self.assertRaises(ValueError, lambda: self.p1[-1])
        self.assertRaises(ValueError, lambda: self.p1[100])

    def test_setitem(self):
        self.assertRaises(ValueError, lambda: self.p1[-1] == 2)
        t1 = Poly(3, 5)
        t1[5] = 9
        self.assertEqual(t1[5], 9)
        t1[10] = 7
        self.assertEqual(t1[10], 7)
        self.assertEqual(len(t1), 11)

    def test_call(self):
        self.assertEqual(self.p1(0), 0)
        self.assertEqual(Poly(2, 2)(2), 8)
        self.assertEqual(Poly(2, 2)(Poly(1, 0)), Poly(2, 0))
        self.assertEqual(Poly(2, 2)(Poly(1, 2)), Poly(2, 4))

    def tearDown(self): pass
