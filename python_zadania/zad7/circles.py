#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from points import Point
import math


class Circle:
    """Klasa reprezentująca okręgi na płaszczyźnie."""

    def __init__(self, x=0, y=0, radius=1):
        if radius < 0:
            raise ValueError("promień ujemny")
        self.pt = Point(x, y)
        self.radius = radius

    def __repr__(self):  # "Circle(x, y, radius)"
        return "Circle(%s, %s, %s)" % (self.pt.x, self.pt.y, self.radius)

    def __eq__(self, other):
        return self.pt == other.pt and self.radius == other.radius

    def __ne__(self, other):
        return not self == other

    def area(self):  # pole powierzchni
        return math.pi * (self.radius ** 2)

    def move(self, x, y):  # przesuniecie o (x, y)
        return Circle(self.pt.x + x, self.pt.y + y, self.radius)

    def cover(self, other):  # okrąg pokrywający oba
        if self.pt.x != other.pt.x:
            #prosta przechodzaca przez srodki okregow
            a = (self.pt.y - other.pt.y) * 1.0 / (self.pt.x - other.pt.x)
            b = self.pt.y - a * self.pt.x

            # punkty przeciecia prostej z okręgiem
            ak = (1 + a ** 2)
            bk = (-2 * self.pt.x + 2 * a * b - 2 * a * self.pt.y)
            ck = (self.pt.x ** 2) - 2 * b * self.pt.y + (self.pt.y ** 2) - (self.radius ** 2) + (b ** 2)
            delta = (bk ** 2) - 4 * ak * ck
            xl = (-bk - math.sqrt(delta)) / (2.0 * ak)
            xp = (-bk + math.sqrt(delta)) / (2.0 * ak)

            bk = (-2 * other.pt.x + 2 * a * b - 2 * a * other.pt.y)
            ck = (other.pt.x ** 2) - 2 * b * other.pt.y + (other.pt.y ** 2) - (other.radius ** 2) + (b ** 2)
            delta = (bk ** 2) - 4 * ak * ck
            xl2 = (-bk - math.sqrt(delta)) / (2.0 * ak)
            xp2 = (-bk + math.sqrt(delta)) / (2.0 * ak)

            #skrajne punkty
            xl = min(xl, xl2)
            xp = max(xp, xp2)

            yl = a * xl + b
            yp = a * xp + b
            sX = (xl + xp) / 2.0
            sY = (yl + yp) / 2.0

            r = math.sqrt((xl - sX) ** 2 + (yl - sY) ** 2)
            return Circle(sX, sY, r)
        else:
            yg = max(self.pt.y + self.radius, other.pt.y + other.radius)
            yd = min(self.pt.y - self.radius, other.pt.y - other.radius)
            yS = (yg + yd) / 2.0
            r = (yg - yd) / 2.0
            return Circle(self.pt.x, yS, r)


# Kod testujący moduł.

import unittest


class TestCircle(unittest.TestCase):
    def setUp(self):
        self.c1 = Circle(0, 0, 1)
        self.c2 = Circle(1, 2, 5)
        self.c3 = Circle(3, 3, 3)

    def test_raise(self):
        self.assertRaises(ValueError, lambda: Circle(0, 0, -1))

    def test_print(self):
        self.assertEqual(repr(self.c1), "Circle(0, 0, 1)")
        self.assertEqual(repr(self.c2), "Circle(1, 2, 5)")
        self.assertEqual(repr(self.c3), "Circle(3, 3, 3)")

    def test_compare(self):
        self.assertTrue(self.c1 == Circle(0, 0, 1))
        self.assertFalse(self.c1 == self.c2)
        self.assertTrue(self.c1 != self.c2)
        self.assertTrue(self.c1 != self.c3)
        self.assertFalse(self.c1 != self.c1)

    def test_area(self):
        self.assertEqual(self.c1.area(), math.pi)
        self.assertEqual(self.c2.area(), 25 * math.pi)
        self.assertEqual(self.c3.area(), 9 * math.pi)

    def test_move(self):
        self.assertEqual(self.c1.move(2, 3), Circle(2, 3, 1))
        self.assertEqual(self.c2.move(-2, 3), Circle(-1, 5, 5))
        self.assertEqual(self.c3.move(2, -3), Circle(5, 0, 3))

    def test_cover(self):
        o1 = Circle(0, 0, 2)
        o2 = Circle(4, 0, 2)
        cov = o1.cover(o2)
        self.assertEqual(cov, Circle(2, 0, 4))
        o2 = Circle(0, 6, 4)
        self.assertEqual(o1.cover(o2), Circle(0, 4, 6))
        self.assertEqual(o2.cover(Circle(0, 5, 1)), o2)

    def tearDown(self): pass
