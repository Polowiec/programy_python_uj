#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

from polys import *

p1 = Poly(5, 2)
p2 = Poly(3, 3)
p3 = p1 + p2
print(p1)
print(+p1)
print(-p1)
print(p1 + p2)
print(p1 + 5)
print(9 + p2)
print(p2 - p1)
print(20 - p1)
print(p2 - 1)
print(p2 * p1)
print(2 * p1)
print(p2 * 3)
print()
print(p2.eval(4))
print(p2.combine(p1))
print(p3 ** 2)
print(p3 == p1)
print(p3.diff())
print(p3.integrate())
print(p3.is_zero())
print()
print(len(p1))
print(p1[2])
p1[10] = 2
print(p1[10])
print(p1(p2))
print(p1(7))
