#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

from math import gcd


class Frac:
    """Klasa reprezentująca ułamek."""

    def normujUlamek(self, frac):
        if frac[0] == 0:
            return [0, 1]
        if frac[1] < 0:
            frac[0] *= -1
            frac[1] *= -1
        skrocenie = gcd(frac[0], frac[1])
        frac[0] /= skrocenie
        frac[1] /= skrocenie
        return frac

    def __init__(self, x=0, y=1):
        x1, x2 = float.as_integer_ratio(x * 1.0)
        y1, y2 = float.as_integer_ratio(y * 1.0)
        x = x1 * y2
        y = x2 * y1
        if y == 0:
            raise ValueError()
        x, y = self.normujUlamek([x, y])
        self.x, self.y = int(x), int(y)

    def __str__(self):  # zwraca "x/y" lub "x" dla y=1
        if self.y == 1:
            return str(self.x)
        else:
            return str(self.x) + "/" + str(self.y)

    def __repr__(self):  # zwraca "Frac(x, y)"
        return "Frac(%s, %s)" % (self.x, self.y)

    def __eq__(self, other):  # python 3.5
        return self.x * other.y == other.x * self.y

    def __lt__(self, other):
        return self.x * other.y < other.x * self.y

    def __cmp__(self, other):  # dla pythona 2.7
        if self == other:
            return 0
        elif self < other:
            return -1
        else:
            return 1

    def __add__(self, other):  # frac1+frac2
        if (isinstance(other, (int, float))):
            x1, x2 = float.as_integer_ratio(other * 1.0)
            other = Frac(x1, x2)

        wsp_mian = self.y * other.y
        licznik = self.x * other.y + other.x * self.y
        return Frac(licznik, wsp_mian)

    __radd__ = __add__

    def __sub__(self, other):  # frac1-frac2
        if (isinstance(other, (int, float))):
            x1, x2 = float.as_integer_ratio(other * 1.0)
            other = Frac(x1, x2)

        wsp_mian = self.y * other.y
        licznik = self.x * other.y - other.x * self.y
        return Frac(licznik, wsp_mian)

    def __rsub__(self, other):
        x1, x2 = float.as_integer_ratio(other * 1.0)
        other = Frac(x1, x2)

        return other - self

    def __mul__(self, other):  # frac1*frac2
        if (isinstance(other, (int, float))):
            x1, x2 = float.as_integer_ratio(other * 1.0)
            other = Frac(x1, x2)

        licznik = self.x * other.x
        mianownik = self.y * other.y
        if mianownik != 0:
            return Frac(licznik, mianownik)

    __rmul__ = __mul__

    def __div__(self, other):  # frac1/frac2
        if (isinstance(other, (int, float))):
            x1, x2 = float.as_integer_ratio(other * 1.0)
            other = Frac(x1, x2)

        licznik = self.x * other.y
        mianownik = self.y * other.x
        if mianownik != 0:
            return Frac(licznik, mianownik)
        else:
            raise ValueError()

    __truediv__ = __div__

    def __rdiv__(self, other):
        if (isinstance(other, (int, float))):
            x1, x2 = float.as_integer_ratio(other * 1.0)
            other = Frac(x1, x2)
        return other / self

    def __rtruediv__(self, other):
        if (isinstance(other, (int, float))):
            x1, x2 = float.as_integer_ratio(other * 1.0)
            other = Frac(x1, x2)
        return other / self

    # operatory jednoargumentowe
    def __pos__(self):  # +frac
        return self

    def __neg__(self):  # -frac
        return Frac(-self.x, self.y)

    def __invert__(self):  # odwrotnosc: ~frac
        return Frac(self.y, self.x)

    def __float__(self):  # float(frac)
        return 1.0 * self.x / self.y


# Kod testujący moduł.

import unittest


class TestFrac(unittest.TestCase):
    def setUp(self):
        self.f1 = Frac(3, 8)
        self.f2 = Frac(2, -8)
        self.f3 = Frac(2, 1)

    def test_0(self):
        self.assertRaises(ValueError, lambda: Frac(1, 0))

    def test_print(self):
        self.assertEqual(str(self.f1), "3/8")
        self.assertEqual(str(self.f2), "-1/4")
        self.assertEqual(str(self.f3), "2")
        self.assertEqual(repr(self.f1), "Frac(3, 8)")
        self.assertEqual(repr(self.f2), "Frac(-1, 4)")
        self.assertEqual(repr(self.f3), "Frac(2, 1)")

    def test_compare(self):
        self.assertTrue(Frac() == Frac(0, 10))
        self.assertTrue(Frac(0, 20) == Frac(0, 10))
        self.assertTrue(Frac(1, 4) == Frac(2, 8))
        self.assertTrue(Frac(-1, 4) == Frac(2, -8))
        self.assertTrue(Frac(-1, -4) == Frac(2, 8))
        self.assertFalse(Frac(-1, -4) == Frac(1, 3))
        self.assertTrue(Frac(1, 3) < Frac(1, 2))
        self.assertFalse(Frac(-1, 3) < Frac(-1, 2))

    def test_add(self):
        self.assertEqual(self.f1 + self.f2, Frac(1, 8))
        self.assertEqual(Frac(1, 2) + Frac(1, 2), Frac(1, 1))
        self.assertEqual(Frac(-1, -2) + Frac(-1, -2), Frac(1, 1))
        self.assertEqual(Frac(1, 8) + Frac(-1, 8), Frac(0, 1))
        self.assertEqual(Frac(1, 8) + Frac(1, -8), Frac(0, 1))

    def test_add_float(self):
        self.assertEqual(Frac(1, 8) + 0.125, Frac(2, 8))
        self.assertEqual(Frac(-1, 8) + 0.125, Frac(0, 8))
        self.assertEqual(0.125 + Frac(1, 8), Frac(2, 8))
        self.assertEqual(0.125 + Frac(-1, 8), Frac(0, 8))

    def test_sub(self):
        self.assertEqual(self.f1 - self.f2, Frac(5, 8))
        self.assertEqual(self.f1 - self.f1, Frac(0, 1))
        self.assertEqual(Frac(2, 3) - Frac(1, 6), Frac(1, 2))
        self.assertEqual(Frac(2, 3) - Frac(1, -6), Frac(5, 6))

    def test_sub_float(self):
        self.assertEqual(self.f1 - 0, self.f1)
        self.assertEqual(self.f1 - 1, Frac(-5, 8))
        self.assertEqual(0 - self.f1, Frac(-3, 8))
        self.assertEqual(0.625 - self.f1, Frac(2, 8))
        self.assertEqual(0.375 - self.f1, Frac(0, 8))

    def test_mul(self):
        self.assertEqual(self.f1 * self.f2, Frac(-6, 64))
        self.assertEqual(Frac(1, -4) * Frac(-8, -1), Frac(-2, 1))
        self.assertRaises(ValueError, lambda: Frac(1, 4) * Frac(1, 0))

    def test_mul_float(self):
        self.assertEqual(0 * self.f1, Frac(0, 1))
        self.assertEqual(self.f1 * 0, Frac(0, 1))
        self.assertEqual(self.f1 * 1, self.f1)
        self.assertEqual(2 * self.f1, Frac(6, 8))

    def test_div(self):
        self.assertEqual(self.f1 / self.f2, Frac(-3, 2))
        self.assertEqual(Frac(1, -4) / Frac(-8, -1), Frac(-1, 32))
        self.assertRaises(ValueError, lambda: Frac(1, 4) / Frac(0, 1))

    def test_deiv_float(self):
        self.assertEqual(0 / self.f1, Frac(0, 1))
        self.assertEqual(0.375 / self.f1, Frac(1, 1))
        self.assertEqual(self.f1 / 0.5, Frac(6, 8))
        self.assertEqual(self.f1 / 1, self.f1)
        self.assertRaises(ValueError, lambda: self.f1 / 0)

    def test_plus(self):
        self.assertEqual(+self.f2, self.f2)
        self.assertEqual(+self.f3, self.f3)

    def test_minus(self):
        self.assertEqual(-self.f1, Frac(-3, 8))
        self.assertEqual(-self.f2, Frac(2, 8))

    def test_invert(self):
        self.assertEqual(~Frac(1, 2), Frac(2, 1))
        self.assertEqual(~Frac(1, -2), Frac(-2, 1))
        self.assertRaises(ValueError, lambda: ~Frac(0, 4))

    def test_float(self):
        self.assertEqual(float(Frac(1, 2)), 0.5)
        self.assertEqual(float(Frac(1, -2)), -0.5)
        self.assertEqual(float(Frac(3, 9)), 1.0 / 3)
        self.assertEqual(float(Frac(0, 9)), 0)

    def tearDown(self): pass


if __name__ == '__main__':
    unittest.main()  # uruchamia wszystkie testy
