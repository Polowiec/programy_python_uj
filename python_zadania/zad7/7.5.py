#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from circles import *

o1 = Circle(0, 1, 2)
o2 = Circle(4, 8, 7)
print(o1)
print(o1 == o2)
print(o2.area())
print(o1.move(2, 4))
print(o1.cover(o2))
