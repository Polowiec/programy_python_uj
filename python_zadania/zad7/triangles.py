#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from points import Point
from math import sqrt


class Triangle:
    """Klasa reprezentująca trójkąt na płaszczyźnie."""

    def __init__(self, x1=0, y1=0, x2=0, y2=0, x3=0, y3=0):
        # Należy zabezpieczyć przed sytuacją, gdy punkty są współliniowe.
        if x1 == x2:
            if x2 == x3:
                raise ValueError
        else:
            a = (y1 - y2) * 1.0 / (x1 - x2)
            b = y1 - a * x1
            if y3 == a * x3 + b:
                raise ValueError

        self.pt1 = Point(x1, y1)
        self.pt2 = Point(x2, y2)
        self.pt3 = Point(x3, y3)

    def __str__(self):  # "[(x1, y1), (x2, y2), (x3, y3)]"
        return "[%s, %s, %s]" % (self.pt1, self.pt2, self.pt3)

    def __repr__(self):  # "Triangle(x1, y1, x2, y2, x3, y3)"
        return "Triangle(%s, %s, %s, %s, %s, %s)" % (
            self.pt1.x, self.pt1.y, self.pt2.x, self.pt2.y, self.pt3.x, self.pt3.y)

    def __eq__(self, other):  # obsługa tr1 == tr2
        if self.pt1 == other.pt1 and self.pt2 == other.pt2 and self.pt3 == other.pt3:
            return True
        if self.pt1 == other.pt1 and self.pt2 == other.pt3 and self.pt3 == other.pt2:
            return True
        if self.pt1 == other.pt2 and self.pt2 == other.pt1 and self.pt3 == other.pt3:
            return True
        if self.pt1 == other.pt2 and self.pt2 == other.pt3 and self.pt3 == other.pt1:
            return True
        if self.pt1 == other.pt3 and self.pt2 == other.pt1 and self.pt3 == other.pt2:
            return True
        if self.pt1 == other.pt3 and self.pt2 == other.pt2 and self.pt3 == other.pt1:
            return True
        return False

    def __ne__(self, other):  # obsługa tr1 != tr2
        return not self == other

    def center(self):  # zwraca środek trójkąta
        return ((self.pt1.x + self.pt2.x + self.pt3.x) / 3.0, (self.pt1.y + self.pt2.y + self.pt3.y) / 3.0)

    def area(self):  # pole powierzchni
        # wzor herona
        a = self.pt1.distance_to(self.pt2)
        b = self.pt1.distance_to(self.pt3)
        c = self.pt3.distance_to(self.pt2)
        p = 0.5 * (a + b + c)
        return sqrt(p * (p - a) * (p - b) * (p - c))

    def move(self, x, y):  # przesunięcie o (x, y)
        return Triangle(self.pt1.x + x, self.pt1.y + y, self.pt2.x + x, self.pt2.y + y, self.pt3.x + x, self.pt3.y + y)

    def make4(self):
        dx = (self.pt1.x - self.pt2.x) / 4.0
        dy = (self.pt1.y - self.pt2.y) / 4.0

        wynik = []
        for i in range(4):
            wynik.append(
                Triangle(self.pt1.x + i * dx, self.pt1.y + i * dy, self.pt1.x + (i + 1) * dx, self.pt1.y + (i + 1) * dy,
                         self.pt3.x, self.pt3.y))
        return wynik


# Kod testujący moduł.

import unittest


class TestTriangle(unittest.TestCase):
    def setUp(self):
        self.t1 = Triangle(0, 0, 0, 4, 3, 0)
        self.t2 = Triangle(0, 2, 3, 4, 5, 6)

    def test_raise(self):
        self.assertRaises(ValueError, lambda: Triangle(1, 1, 2, 2, 3, 3))
        self.assertRaises(ValueError, lambda: Triangle(1, 1, 1, 2, 1, 3))

    def test_print(self):
        self.assertEqual(str(self.t1), "[(0, 0), (0, 4), (3, 0)]")
        self.assertEqual(str(self.t2), "[(0, 2), (3, 4), (5, 6)]")
        self.assertEqual(repr(self.t1), "Triangle(0, 0, 0, 4, 3, 0)")
        self.assertEqual(repr(self.t2), "Triangle(0, 2, 3, 4, 5, 6)")

    def test_equal(self):
        self.assertFalse(self.t1 == self.t2)
        self.assertTrue(self.t1 == self.t1)
        self.assertTrue(self.t1 == Triangle(3, 0, 0, 4, 0, 0))
        self.assertTrue(self.t1 == Triangle(0, 0, 0, 4, 3, 0))
        self.assertTrue(self.t1 == Triangle(0, 0, 3, 0, 0, 4))

    def test_center(self):
        self.assertEqual(self.t1.center(), (1, 4.0 / 3))
        self.assertEqual(self.t2.center(), (8.0 / 3, 4))

    def test_area(self):
        self.assertEqual(self.t1.area(), 6)
        self.assertAlmostEqual(self.t2.area(), 1, 2)

    def test_move(self):
        self.assertEqual(self.t1.move(0, 0), self.t1)
        self.assertEqual(self.t1.move(2, 5), Triangle(2, 5, 2, 9, 5, 5))

    def test_make4(self):
        w = self.t1.make4()
        pole = 0
        for x in w:
            pole += x.area()
        self.assertAlmostEqual(pole, self.t1.area(), 2)
        w = self.t2.make4()
        pole = 0
        for x in w:
            pole += x.area()
        self.assertAlmostEqual(pole, self.t2.area(), 2)

    def tearDown(self):
        pass


if __name__ == "__main__":
    unittest.main()  # wszystkie testy
