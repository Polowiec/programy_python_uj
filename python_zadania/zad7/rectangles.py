#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from points import Point


class Rectangle:
    """Klasa reprezentująca prostokąt na płaszczyźnie."""

    def __init__(self, x1=0, y1=0, x2=0, y2=0):
        # Chcemy, aby x1 <= x2, y1 <= y2.
        if x1 > x2 or y1 > y2:
            raise ValueError
        self.pt1 = Point(x1, y1)
        self.pt2 = Point(x2, y2)

    def __str__(self):  # "[(x1, y1), (x2, y2)]"
        return "[%s, %s]" % (self.pt1, self.pt2)

    def __repr__(self):  # "Rectangle(x1, y1, x2, y2)"
        return "Rectangle(%s, %s, %s, %s)" % (self.pt1.x, self.pt1.y, self.pt2.x, self.pt2.y)

    def __eq__(self, other):  # obsługa rect1 == rect2
        # uwzledniam przypadek gdy punkty są odane na odwrot, lub jest podana druga para punktow prostokata
        return self.pt1 == other.pt1 and self.pt2 == other.pt2

    def __ne__(self, other):  # obsługa rect1 != rect2
        return not self == other

    def center(self):  # zwraca środek prostokąta
        return ((self.pt1.x + self.pt2.x) / 2.0, (self.pt1.y + self.pt2.y) / 2.0)

    def area(self):  # pole powierzchni
        return abs((self.pt1.x - self.pt2.x) * (self.pt1.y - self.pt2.y))

    def move(self, x, y):  # przesunięcie o (x, y)
        return Rectangle(self.pt1.x + x, self.pt1.y + y, self.pt2.x + x, self.pt2.y + y)

    def intersection(self, other):  # część wspólna prostokątów
        x1 = max(self.pt1.x, other.pt1.x);
        x2 = min(self.pt2.x, other.pt2.x);
        y1 = max(self.pt1.y, other.pt1.y);
        y2 = min(self.pt2.y, other.pt2.y);
        if x2 >= x1 and y2 >= y1:
            return Rectangle(x1, y1, x2, y2);
        else:
            raise ValueError

    def cover(self, other):  # prostąkąt nakrywający oba
        xmin = min(self.pt1.x, other.pt1.x)
        ymin = min(self.pt1.y, other.pt1.y)
        xmax = max(self.pt2.x, other.pt2.x)
        ymax = max(self.pt2.y, other.pt2.y)
        return Rectangle(xmin, ymin, xmax, ymax)

    def make4(self):  # zwraca listę czterech mniejszych
        xSr = (self.pt1.x + self.pt2.x) / 2.0
        ySr = (self.pt1.y + self.pt2.y) / 2.0
        return [Rectangle(self.pt1.x, self.pt1.y, xSr, ySr), Rectangle(self.pt1.x, ySr, xSr, self.pt2.y),
                Rectangle(xSr, self.pt1.y, self.pt2.x, ySr), Rectangle(xSr, ySr, self.pt2.x, self.pt2.y)]


# Kod testujący moduł.

import unittest


class TestRectangle(unittest.TestCase):
    def setUp(self):
        self.r1 = Rectangle(1, 1, 4, 4)
        self.r2 = Rectangle(-1, 3, 5, 8)

    def test_raise(self):
        self.assertRaises(ValueError, lambda: Rectangle(9, 9, 0, 0))

    def test_print(self):
        self.assertEqual(str(self.r1), "[(1, 1), (4, 4)]")
        self.assertEqual(str(self.r2), "[(-1, 3), (5, 8)]")
        self.assertEqual(repr(self.r1), "Rectangle(1, 1, 4, 4)")
        self.assertEqual(repr(self.r2), "Rectangle(-1, 3, 5, 8)")

    def test_equal(self):
        self.assertTrue(self.r1 == self.r1)
        self.assertTrue(self.r1 == Rectangle(1, 1, 4, 4))
        self.assertFalse(self.r1 == self.r2)
        self.assertTrue(self.r1 != self.r2)

    def test_center(self):
        self.assertEqual(self.r1.center(), (2.5, 2.5))
        self.assertEqual(self.r2.center(), (2, 5.5))

    def test_area(self):
        self.assertEqual(self.r1.area(), 9)
        self.assertEqual(self.r2.area(), 30)

    def test_move(self):
        self.assertEqual(self.r1.move(0, 0), self.r1)
        self.assertEqual(self.r2.move(0, 0), self.r2)
        self.assertEqual(self.r1.move(-2, -5), Rectangle(-1, -4, 2, -1))
        self.assertEqual(self.r2.move(4, -10), Rectangle(3, -7, 9, -2))

    def test_intersection(self):
        self.assertEqual(Rectangle(1, 1, 5, 5).intersection(Rectangle(2, 2, 3, 3)), Rectangle(2, 2, 3, 3))
        self.assertEqual(self.r1.intersection(self.r1), self.r1)
        self.assertEqual(Rectangle(1, 1, 5, 5).intersection(Rectangle(2, 2, 8, 8)), Rectangle(2, 2, 5, 5))
        self.assertEqual(Rectangle(1, 1, 5, 5).intersection(Rectangle(5, 5, 8, 8)), Rectangle(5, 5, 5, 5))
        self.assertEqual(Rectangle(1, 1, 5, 5).intersection(Rectangle(2, 2, 7, 4)), Rectangle(2, 2, 5, 4))
        self.assertRaises(ValueError, lambda: Rectangle(1, 1, 5, 5).intersection(Rectangle(20, 20, 70, 40)))

    def test_cover(self):
        self.assertEqual(Rectangle(1, 1, 3, 3).cover(Rectangle(2, 1, 4, 5)), Rectangle(1, 1, 4, 5))
        self.assertEqual(Rectangle(1, 1, 3, 3).cover(Rectangle(20, 10, 40, 50)), Rectangle(1, 1, 40, 50))
        self.assertEqual(self.r1.cover(self.r1), self.r1)

    def test_make4(self):
        rects = [Rectangle(0, 0, 4, 4), Rectangle(0, 2, 14, 4), Rectangle(-2, -8, 4, 14)]
        for x in rects:
            mniejsze = x.make4()
            pole = 0
            for y in mniejsze:
                pole += y.area()
        self.assertEqual(pole, x.area())

    def tearDown(self):
        pass


if __name__ == "__main__":
    unittest.main()  # wszystkie testy
