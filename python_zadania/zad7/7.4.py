#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from triangles import *

t1 = Triangle(0, 0, 0, 4, 3, 0)
t2 = Triangle(0, 2, 3, 4, 5, 6)

print(str(t1))
print(repr(t2))
print(t1 == t2)
print(t2.center())
print(t1.area())
print(t2.move(3, 6))
print(t1.make4())

