#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from math import sqrt

class Point:
    """Klasa reprezentująca punkty na płaszczyźnie."""

    def __init__(self, x=0, y=0):  # konstuktor
        self.x = x
        self.y = y

    def __str__(self):  # zwraca string "(x, y)"
        return str((self.x, self.y))

    def __repr__(self):  # zwraca string "Point(x, y)"
        return "Point(%s, %s)" % (self.x, self.y)

    def __eq__(self, other):  # obsługa point1 == point2
        return self.x == other.x and self.y == other.y

    def __ne__(self, other):  # obsługa point1 != point2
        return not self == other

    def distance_to(self, other):
        return sqrt( (self.x - other.x)*(self.x - other.x) + (self.y - other.y)*(self.y - other.y) )



class Vector(Point):
    """Klasa reprezentująca wektory na płaszczyźnie."""

    # __init__ dziedziczone
    # __str__ dziedziczone
    def __repr__(self):  # zwraca string "Vector(x, y)"
        return "Vector(%s, %s)" % (self.x, self.y)

    def __add__(self, other):  # v1 + v2
        return Vector(self.x + other.x, self.y + other.y)

    def __sub__(self, other):  # v1 - v2
        return Vector(self.x - other.x, self.y - other.y)

    def __mul__(self, other):  # v1 * v2, iloczyn skalarny
        return self.x * other.x + self.y * other.y

    def cross(self, other):  # v1 x v2, iloczyn wektorowy
        return (0, 0, self.x * other.y - self.y * other.x)

    def length(self):  # długość wektora
        return sqrt(self.x * self.x + self.y * self.y)


# Kod testujący moduł.

import unittest


class TestPoint(unittest.TestCase):
    def setUp(self): pass

    def test_print(self):
        self.assertEqual(str(Point(3, 4)), "(3, 4)")
        self.assertEqual(str(Point(-3, -13)), "(-3, -13)")
        self.assertEqual(repr(Point(3, 4)), "Point(3, 4)")
        self.assertEqual(repr(Point(-3, -13)), "Point(-3, -13)")

    def test_equal_nequal(self):
        self.assertTrue(Point(2, 3) == Point(2, 3))
        self.assertFalse(Point(2, 3) == Point(2, 8))
        self.assertFalse(Point(2, 3) == Point(3, 3))
        self.assertTrue(Point(2, 3) != Point(3, 3))
        self.assertTrue(Point(-3, 3) != Point(3, 3))
        self.assertFalse(Point(2, 3) != Point(2, 3))

    def test_distance_to(self):
        self.assertEqual(Point(0, 0).distance_to(Point(3, 4)), 5)
        self.assertEqual(Point(-3, 0).distance_to(Point(-3, 4)), 4)
        self.assertEqual(Point(1, 3).distance_to(Point(-3, 0)), 5)

    def tearDown(self): pass


class TestVector(unittest.TestCase):
    def setUp(self): pass

    def test_print(self):
        self.assertEqual(str(Vector(1, 3)), "(1, 3)")
        self.assertEqual(str(Vector(-1, 8)), "(-1, 8)")
        self.assertEqual(repr(Vector(1, 3)), "Vector(1, 3)")
        self.assertEqual(repr(Vector(-1, 8)), "Vector(-1, 8)")

    def test_add(self):
        self.assertEqual(Vector(1, 2) + Vector(4, 7), Vector(5, 9))
        self.assertEqual(Vector(1, 2) + Vector(4, -7), Vector(5, -5))

    def test_sub(self):
        self.assertEqual(Vector(1, 2) - Vector(4, 7), Vector(-3, -5))
        self.assertEqual(Vector(1, -2) - Vector(4, -7), Vector(-3, 5))

    def test_mul(self):
        self.assertEqual(Vector(2, 3) * Vector(1, 3), 11)
        self.assertEqual(Vector(2, 3) * Vector(1, -3), -7)
        self.assertEqual(Vector(1, 0) * Vector(0, -3), 0)

    def test_cross(self):
        self.assertEqual(Vector(1, 1).cross(Vector(1, 1)), (0, 0, 0))
        self.assertEqual(Vector(3, 1).cross(Vector(2, 4)), (0, 0, 10))

    def test_length(self):
        self.assertEqual(Vector(3, 4).length(), 5)
        self.assertEqual(Vector(6, 8).length(), 10)

    def tearDown(self): pass


if __name__ == "__main__":
    unittest.main()  # wszystkie testy
