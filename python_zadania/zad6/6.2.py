#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from points import *

print(str(Point(-3, -13)))
print(repr(Point(3, 4)))
print(Point(1, 2) == Point(5, 8))
print(Point(1, 2) != Point(5, 8))

print(str(Vector(-3, -13)))
print(repr(Vector(3, 4)))
print(Vector(1, 2) + Vector(5, 8))
print(Vector(1, 2) - Vector(5, 8))
print(Vector(1, 2) * Vector(5, 8))
print(Vector(1, 2).cross(Vector(5, 8)))
print(Vector(1, 2).length())
