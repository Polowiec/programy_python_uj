#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


class Time:
    """Klasa reprezentująca odcinek czasu."""

    def __init__(self, s=0):
        """Zwraca instancję klasy Time."""
        self.s = int(s)

    def __str__(self):
        """Zwraca string 'hh:mm:ss'."""
        h = int(self.s / 3600)
        sec = int(self.s - h * 3600)
        m = int(sec / 60)
        sec = int(sec - m * 60)
        return "%s:%s:%s" % (str(h).zfill(2),
                             str(m).zfill(2), str(sec).zfill(2))

    def __repr__(self):
        """Zwraca string 'Time(s)'."""
        return "Time(%s)" % self.s

    def __add__(self, other):
        """Dodawanie odcinków czasu."""
        return Time(self.s + other.s)

    def __cmp__(self, other):  # porównywanie, -1|0|+1, dla pythona 2
        """Porównywanie odcinków czasu."""
        return cmp(self.s, other.s)

    def __eq__(self, other):  # dla pythona 3
        return self.s == other.s

    def __lt__(self, other):  # dla pythona 3
        return self.s < other.s

    def __int__(self):  # int(time1)
        """Konwersja odcinka czasu do int."""
        return self.s


# Kod testujący moduł.

import unittest


class TestTime(unittest.TestCase):
    def setUp(self): pass

    def test_print(self):  # test str() i repr()
        self.assertEqual(repr(Time()), "Time(0)")
        self.assertEqual(repr(Time(0)), "Time(0)")
        self.assertEqual(repr(Time(5)), "Time(5)")
        self.assertEqual(repr(Time(150)), "Time(150)")
        self.assertEqual(str(Time()), "00:00:00")
        self.assertEqual(str(Time(0)), "00:00:00")
        self.assertEqual(str(Time(5)), "00:00:05")
        self.assertEqual(str(Time(59)), "00:00:59")
        self.assertEqual(str(Time(120)), "00:02:00")
        self.assertEqual(str(Time(150)), "00:02:30")
        self.assertEqual(str(Time(8 * 60 * 60)), "08:00:00")
        self.assertEqual(str(Time(12 * 60 * 60 + 150)), "12:02:30")

    def test_add(self):
        self.assertEqual(Time(1) + Time(2), Time(3))
        self.assertEqual(Time(1) + Time(8), Time(9))

    def test_cmp(self):
        # Można sprawdzać ==, !=, >, >=, <, <=.
        self.assertTrue(Time(1) == Time(1))
        self.assertTrue(Time(1) != Time(2))
        self.assertTrue(Time(3) > Time(2))

    def test_int(self):
        self.assertEqual(int(Time()), 0)
        self.assertEqual(int(Time(0)), 0)
        self.assertEqual(int(Time(12)), 12)
        self.assertEqual(int(Time(153)), 153)

    def tearDown(self): pass


if __name__ == "__main__":
    unittest.main()  # wszystkie testy
