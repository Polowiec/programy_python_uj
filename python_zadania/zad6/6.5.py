#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Tomasz Polowiec'

from fracs import *

f1= Frac(3, 4)
f2= Frac(2, 5)

print(str(f1))
print(repr(f2))
print(f1 == f2)
print( f2 < f1)
print(f1 + f2)
print(f1 - f2)
print(f1 * f2)
print(f1 / f2)
print(+f1)
print(-f1)
print(~f1)
print(float(f1))