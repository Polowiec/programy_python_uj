#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from points import Point


class Rectangle:
    """Klasa reprezentująca prostokąt na płaszczyźnie."""

    def __init__(self, x1=0, y1=0, x2=0, y2=0):
        self.pt1 = Point(x1, y1)
        self.pt2 = Point(x2, y2)

    def __str__(self):  # "[(x1, y1), (x2, y2)]"
        return "[%s, %s]" % (self.pt1, self.pt2)

    def __repr__(self):  # "Rectangle(x1, y1, x2, y2)"
        return "Rectangle(%s, %s, %s, %s)" % (self.pt1.x, self.pt1.y, self.pt2.x, self.pt2.y)

    def __eq__(self, other):  # obsługa rect1 == rect2
        # uwzledniam przypadek gdy punkty są odane na odwrot, lub jest podana druga para punktow prostokata
        return min(self.pt1.x, self.pt2.x) == min(other.pt1.x, other.pt2.x) and min(self.pt1.y, self.pt2.y) == min(
            other.pt1.y, other.pt2.y) and max(self.pt1.x, self.pt2.x) == max(other.pt1.x, other.pt2.x) and max(
            self.pt1.y, self.pt2.y) == max(other.pt1.y, other.pt2.y)

    def __ne__(self, other):  # obsługa rect1 != rect2
        return not self == other

    def center(self):  # zwraca środek prostokąta
        return ((self.pt1.x + self.pt2.x) / 2.0, (self.pt1.y + self.pt2.y) / 2.0)

    def area(self):  # pole powierzchni
        return abs((self.pt1.x - self.pt2.x) * (self.pt1.y - self.pt2.y))

    def move(self, x, y):  # przesunięcie o (x, y)
        return Rectangle(self.pt1.x + x, self.pt1.y + y, self.pt2.x + x, self.pt2.y + y)


# Kod testujący moduł.

import unittest


class TestRectangle(unittest.TestCase):
    def setUp(self):
        self.r1 = Rectangle(1, 1, 4, 4)
        self.r2 = Rectangle(-1, 3, 5, 8)

    def test_print(self):
        self.assertEqual(str(self.r1), "[(1, 1), (4, 4)]")
        self.assertEqual(str(self.r2), "[(-1, 3), (5, 8)]")
        self.assertEqual(repr(self.r1), "Rectangle(1, 1, 4, 4)")
        self.assertEqual(repr(self.r2), "Rectangle(-1, 3, 5, 8)")

    def test_equal(self):
        self.assertTrue(self.r1 == self.r1)
        self.assertTrue(self.r1 == Rectangle(1, 1, 4, 4))
        self.assertTrue(self.r1 == Rectangle(4, 4, 1, 1))
        self.assertTrue(self.r1 == Rectangle(1, 4, 4, 1))
        self.assertTrue(self.r1 == Rectangle(4, 1, 1, 4))
        self.assertFalse(self.r1 == Rectangle(4, 1, 1, 3))
        self.assertFalse(self.r1 == self.r2)
        self.assertTrue(self.r1 != self.r2)

    def test_center(self):
        self.assertEqual(self.r1.center(), (2.5, 2.5))
        self.assertEqual(self.r2.center(), (2, 5.5))

    def test_area(self):
        self.assertEqual(self.r1.area(), 9)
        self.assertEqual(self.r2.area(), 30)

    def test_move(self):
        self.assertEqual(self.r1.move(0, 0), self.r1)
        self.assertEqual(self.r2.move(0, 0), self.r2)
        self.assertEqual(self.r1.move(-2, -5), Rectangle(-1, -4, 2, -1))
        self.assertEqual(self.r2.move(4, -10), Rectangle(3, -7, 9, -2))

    def tearDown(self): pass


if __name__ == "__main__":
    unittest.main()  # wszystkie testy
