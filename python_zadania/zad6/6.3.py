#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from rectangles import *

r1 = Rectangle(0, 0, 4, 5)
r2 = Rectangle(-3, -8, 4, 5)

print(str(r1))
print(repr(r2))
print(r1 == r2)
print(r1 != r2)
print(r1.center())
print(r2.area())
print(r1.move(3, 5))
