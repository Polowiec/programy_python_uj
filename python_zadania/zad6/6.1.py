#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from times import *

t1 = Time(72)
t2 = Time(139)
print(str(t2))
print(repr(t2))
print(t1 + t2)
print(int(t2))
print(Time(13) > Time(8))
