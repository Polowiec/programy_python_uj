#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


# odpowiednik cmp ktorego nie ma juz w pythonie 3
def defaultCmp(a, b):
    if a > b:
        return 1
    elif a == b:
        return 0
    else:
        return -1


class PriorityQueuePython:
    def __init__(self, cmpfunc=defaultCmp):
        self.items = []
        self.cmpfunc = cmpfunc

    def __str__(self):  # podglądamy kolejkę
        return str(self.items)

    def is_empty(self):
        return not self.items

    def insert(self, item):
        self.items.append(item)

    def increase(self, value):
        for x in range(len(self.items)):
            self.items[x] += value

    def remove(self):
        if self.items == []:
            raise ValueError
        maxi = 0
        for i in range(1, len(self.items)):
            if self.cmpfunc(self.items[i], self.items[maxi]) == 1:
                maxi = i
        self.items[maxi], self.items[-1] = self.items[-1], self.items[maxi]
        wart = self.items[-1]
        del self.items[-1]
        return wart


class PriorityQueueTable:
    def __init__(self, size=10):
        self.items = size * [None]
        self.n = 0  # pierwszy wolny indeks
        self.size = size

    def is_empty(self):
        return self.n == 0

    def is_full(self):
        return self.size == self.n

    def insert(self, data):
        if self.is_full():
            raise OverflowError
        self.items[self.n] = data
        self.n = self.n + 1

    def increase(self, value):
        for x in range(self.n):
            self.items[x] += value

    def remove(self):
        if self.is_empty():
            raise ValueError
        # Etap 1 - wyszukiwanie elementu.
        maxi = 0
        for i in range(self.n):
            if self.items[i] > self.items[maxi]:
                maxi = i
        # Etap 2 - usuwanie elementu.
        self.n = self.n - 1
        data = self.items[maxi]
        self.items[maxi] = self.items[self.n]
        self.items[self.n] = None  # usuwamy referencję
        return data


import unittest


class TestPriorityQueuePython(unittest.TestCase):
    def setUp(self):
        self.q = PriorityQueuePython()

    def test_emptyQueue(self):
        self.assertTrue(self.q.is_empty())
        self.assertRaises(ValueError, lambda: self.q.remove())

    def test_removeQueue(self):
        values = [4, -3, 15, -3, 12, 9, 5]
        for x in values:
            self.q.insert(x)
        values.sort(reverse=True)
        for x in values:
            self.assertEqual(self.q.remove(), x)
        self.assertTrue(self.q.is_empty())

    def test_increase(self):
        values = [4, -3, 15, -3, 12, 9, 5]
        for x in values:
            self.q.insert(x)
        values.sort(reverse=True)
        self.q.increase(3)
        for x in values:
            self.assertEqual(self.q.remove(), x + 3)

    def tearDown(self):
        pass


class TestPriorityQueueTable(unittest.TestCase):
    def setUp(self):
        self.rozmiar = 5
        self.q = PriorityQueueTable(self.rozmiar)

    def test_insert(self):
        self.assertTrue(self.q.is_empty())
        self.assertFalse(self.q.is_full())
        values = [4, -3, 15, -3, 12]
        for x in values:
            self.q.insert(x)
        self.assertFalse(self.q.is_empty())
        self.assertTrue(self.q.is_full())
        self.assertRaises(OverflowError, lambda: self.q.insert(0))

    def test_increase(self):
        values = [4, -3, 15, -3, 12]
        for x in values:
            self.q.insert(x)
        values.sort(reverse=True)
        self.q.increase(3)
        for x in values:
            self.assertEqual(self.q.remove(), x + 3)

    def test_remove(self):
        self.assertRaises(ValueError, lambda: self.q.remove())
        values = [4, -3, 15, -3, 12]
        for x in values:
            self.q.insert(x)
        values.sort(reverse=True)
        for x in values:
            self.assertEqual(self.q.remove(), x)
        self.assertTrue(self.q.is_empty())
        self.assertRaises(ValueError, lambda: self.q.remove())

    def tearDown(self):
        pass


if __name__ == "__main__":
    unittest.main()  # wszystkie testy
