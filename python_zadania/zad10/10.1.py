#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from matrix import *

# Zastosowanie.
m = Matrix(3, 4)
m[1, 2] = 12  # metoda __setitem__
print(m[1, 2])  # metoda __getitem__
print(m)  # metoda __str__

mj = Matrix(4, 4)
for x in range(4):
    mj[x, x] = 1

m2 = Matrix(3, 4)
m2[2, 2] = 5

print()
print(m + m2)
print()
print(m - m2)
print()
print(m * mj)
