#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

import random


class RandomQueue:
    def __init__(self):
        self.items = []
        self.size = 0

    def insert(self, item):
        self.items.append(item)
        self.size += 1

    def remove(self):  # zwraca losowy element
        if self.items == []:
            raise ValueError
        index = random.randint(0, self.size - 1)
        self.items[index], self.items[-1] = self.items[-1], self.items[index]
        wart = self.items[-1]
        del self.items[-1]
        self.size -= 1
        return wart

    def is_empty(self):
        return not self.items


import unittest


class TestRandomQueue(unittest.TestCase):
    def setUp(self):
        self.q = RandomQueue()

    def test_empty(self):
        self.assertTrue(self.q.is_empty())
        self.assertRaises(ValueError, lambda: self.q.remove())
        self.q.insert(0)
        self.assertFalse(self.q.is_empty())
        self.q.remove()
        self.assertTrue(self.q.is_empty())
        self.assertRaises(ValueError, lambda: self.q.remove())

    def test_random(self):
        values = [9, 3, 2, 5, -2, 1]
        for x in values:
            self.q.insert(x)

        while not self.q.is_empty():
            elem = self.q.remove()
            self.assertTrue(elem in values)
            del values[values.index(elem)]
        self.assertTrue(self.q.is_empty())

    def tearDown(self):
        pass


if __name__ == "__main__":
    unittest.main()  # wszystkie testy
