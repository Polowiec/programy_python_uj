#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from queue import *

q = Queue(10)
q.put(4)
q.put(2)
q.put(8)
q.put(13)
q.put(-5)
q.put(4)
q.put(-1)
q.put(0)

while not q.is_empty():
    print(q.get())
