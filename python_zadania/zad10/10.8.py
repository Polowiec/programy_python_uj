#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from randomqueue import *

values = [9, 3, 2, 5, -2, 1, 3, 12]
q1= RandomQueue()
for x in values:
    q1.insert(x)

while not q1.is_empty():
    print(q1.remove())