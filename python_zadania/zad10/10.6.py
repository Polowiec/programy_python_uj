#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from priorityqueue import *

values = [4, -3, 15, -3, 12, 9, 5]
q1 = PriorityQueuePython()
q2 = PriorityQueueTable()

for x in values:
    q1.insert(x)
    q2.insert(x)

q1.increase(1)
q2.increase(3)

print("Python Queue")
while not q1.is_empty():
    print(q1.remove())

print()
print("Table Queueu")
while not q2.is_empty():
    print(q2.remove())
