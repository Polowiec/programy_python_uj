#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from stack import *

astack = Stack()
for item in ["a", 2, 3.5]:
    astack.push(item)
# Zdejmowanie elementów ze stosu w kolejności 3.5, 2, a.
while not astack.is_empty():
    print(astack.pop())
