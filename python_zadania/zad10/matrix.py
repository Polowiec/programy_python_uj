#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


class Matrix:
    def __init__(self, rows=1, cols=1):
        self.rows = rows
        self.cols = cols
        self.data = [0] * rows * cols

    def __str__(self):
        wynik = ""
        for w in range(self.rows):
            if w > 0:
                wynik += "\n"
            for k in range(self.cols):
                wynik += str(self.data[w * self.cols + k]).rjust(5)
        return wynik

    def __getitem__(self, pair):  # odczyt m[i,j]
        i, j = pair
        if i < 0 or i >= self.cols or j < 0 or j >= self.rows:
            raise ValueError
        return self.data[i * self.cols + j]

    def __setitem__(self, pair, value):  # m[i,j] = value
        i, j = pair
        if i < 0 or i >= self.cols or j < 0 or j >= self.rows:
            raise ValueError
        self.data[i * self.cols + j] = value

    def __add__(self, other):  # dodawanie macierzy
        if self.rows != other.rows or self.cols != other.cols:
            raise ValueError
        m = Matrix(self.rows, self.cols)
        for x in range(self.cols * self.rows):
            m.data[x] = self.data[x] + other.data[x]
        return m

    def __sub__(self, other):  # odejmowanie macierzy
        if self.rows != other.rows or self.cols != other.cols:
            raise ValueError
        m = Matrix(self.rows, self.cols)
        for x in range(self.cols * self.rows):
            m.data[x] = self.data[x] - other.data[x]
        return m

    def __mul__(self, other):  # mnożenie macierzy
        if self.cols != other.rows:
            raise ValueError

        m = Matrix(self.rows, other.cols)
        for w in range(self.rows):
            for k in range(self.cols):
                for i in range(self.cols):
                    m.data[w * m.cols + k] += self.data[w * self.cols + i] * other.data[i * other.rows + k]
        return m
