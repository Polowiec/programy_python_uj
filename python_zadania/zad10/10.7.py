#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from priorityqueue import *


def reverseCmp(a, b):
    if a > b:
        return -1
    elif a == b:
        return 0
    else:
        return 1


values = [4, -3, 15, -3, 12, 9, 5]

q1 = PriorityQueuePython()
q2 = PriorityQueuePython(reverseCmp)

for x in values:
    q1.insert(x)
    q2.insert(x)

print("Default cmp")
while not q1.is_empty():
    print(q1.remove())

print()
print("Reverse cmp")
while not q2.is_empty():
    print(q2.remove())
