#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


class Stack:
    def __init__(self, size=10):
        self.items = size * [None]  # utworzenie tablicy
        self.n = 0  # liczba elementów na stosie
        self.size = size

    def is_empty(self):
        return self.n == 0

    def is_full(self):
        return self.size == self.n

    def push(self, data):
        if self.n == self.size:
            raise OverflowError()
        self.items[self.n] = data
        self.n = self.n + 1

    def pop(self):
        if self.n == 0:
            raise ValueError
        self.n = self.n - 1
        data = self.items[self.n]
        self.items[self.n] = None  # usuwam referencję
        return data


import unittest


class TestStack(unittest.TestCase):
    def setUp(self):
        self.rozmiar = 10
        self.s = Stack(self.rozmiar)

    def testPush(self):
        self.assertEqual(self.s.is_empty(), True)
        self.assertEqual(self.s.is_full(), False)
        for x in range(self.rozmiar - 1):
            self.s.push(x)
            self.assertEqual(self.s.is_empty(), False)
            self.assertEqual(self.s.is_full(), False)
        self.s.push(-1)
        self.assertEqual(self.s.is_empty(), False)
        self.assertEqual(self.s.is_full(), True)
        self.assertRaises(OverflowError, lambda: self.s.push(0))

    def testPop(self):
        self.assertRaises(ValueError, lambda: self.s.pop())
        for x in range(self.rozmiar):
            self.s.push(x)
        for x in range(self.rozmiar):
            self.assertEqual(self.s.pop(), self.rozmiar - x - 1)
        self.assertTrue(self.s.is_empty())
        self.assertRaises(ValueError, lambda: self.s.pop())

    def tearDown(self):
        pass


if __name__ == "__main__":
    unittest.main()  # wszystkie testy
