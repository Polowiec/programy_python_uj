#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from stack_without_duplicate import *

astack = Stack()
for item in [5, 2, 7]:
    astack.push(item)

while not astack.is_empty():
    print(astack.pop())
