#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

# sciezka do gnuplota
gnuplot_path = '"C:/Program Files (x86)/gnuplot/bin/gnuplot.exe"';


def selectsort(L, left, right):
    for i in range(left, right):
        zapiszDoPliku(L)
        tworzWykres()
        k = i
        for j in range(i + 1, right + 1):
            if L[j] < L[k]:
                k = j
        swap(L, i, k)
    zapiszDoPliku(L)
    tworzWykres()


def swap(L, i, k):
    L[i], L[k] = L[k], L[i]


def zapiszDoPliku(L):
    f = open('stan_sortowania.txt', 'w')
    for x in range(len(L)):
        f.write(str(x) + " " + str(L[x]) + "\n")
    f.close()


import os

ilosc = 1


def tworzWykres():
    global ilosc
    os.system(gnuplot_path + " sort1png.gnu")
    os.system("move sort.png sort" + str(ilosc) + ".png")
    os.system("move sort" + str(ilosc) + ".png \img")
    print("move sort" + str(ilosc) + ".png \img")
    ilosc += 1
