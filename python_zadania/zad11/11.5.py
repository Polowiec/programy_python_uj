#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from countingsort import *
from generatelist import *

lista = losowaLista(20)
lista2 = powtarzajaceZZakresuLista(20, 5)

print(lista)
countingsort(lista, 20)
print(lista)

print()
print(lista2)
countingsort(lista2, 5)
print(lista2)
