#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from generatelist import *
from selectsort_cmpfunc import *

rozmiar = 10
lista = losowaLista(rozmiar)
selectsort(lista, 0, rozmiar - 1)
print(lista)


def reverse_cmp(a, b):
    if a > b:
        return -1
    elif a == b:
        return 0
    else:
        return 1


lista = losowaLista(rozmiar)
selectsort(lista, 0, rozmiar - 1, reverse_cmp)
print(lista)
