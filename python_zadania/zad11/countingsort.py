#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


# sortowanie ze zliczanie,
# złożoność liniowa sortowania
# można sortować tylko zbiór liczb naturalnych od 0 do k-1
# górna granica ( liczba k ) musi być z góry znana
# algorytm zlicza wystąpnienia kolejnych różnych liczb i potem odtwarza posortowaną tablice

def countingsort(L, zakres):
    wystapienia = [0 for x in range(zakres)]
    for x in L:
        wystapienia[x] += 1

    suma = 0
    for i in range(zakres):
        wystapienia[i], suma = suma, suma + wystapienia[i]

    posortowana = [0 for x in range(len(L))]
    for x in L:
        posortowana[wystapienia[x]] = x
        wystapienia[x] += 1
    for i in range(len(L)):
        L[i] = posortowana[i]
