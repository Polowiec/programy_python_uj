set term png
set output "sort.png"

set title "Sortowanie X"
set xlabel "numer pozycji"              # opis osi x
set ylabel "liczba na pozycji"          # opis osi y
unset key                               # bez legendy

plot "stan_sortowania.txt" using 1:2 with points pt 5