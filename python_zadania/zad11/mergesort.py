#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


def merge(L, left, middle, right):
    T = []
    left1 = left
    right1 = middle
    left2 = middle + 1
    right2 = right
    while left1 <= right1 and left2 <= right2:
        if L[left1] <= L[left2]:  # mniejsze lub równe
            T.append(L[left1])
            left1 = left1 + 1
        else:
            T.append(L[left2])
            left2 = left2 + 1
    # Lewa lub prawa część może mieć elementy.
    while left1 <= right1:
        T.append(L[left1])
        left1 = left1 + 1
    while left2 <= right2:
        T.append(L[left2])
        left2 = left2 + 1
    # Skopiuj z tablicy tymczasowej do oryginalnej.
    for i in range(left, right + 1):
        L[i] = T[i - left]


def mergesort(L, left, right):
    if left < right:
        middle = int((left + right) / 2)  # wyznaczanie środka
        mergesort(L, left, middle)
        mergesort(L, middle + 1, right)
        merge(L, left, middle, right)  # scalanie
