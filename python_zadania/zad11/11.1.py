#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from generatelist import *

print(losowaLista(20))
print(losowaLista(6))
print()
print(prawiePosortowanaLista(20))
print(prawiePosortowanaLista(6))
print()
print(prawieOdwrotniePosortowanaLista(20))
print(prawieOdwrotniePosortowanaLista(6))
print()
print(losowyGaussLista(20))
print(losowyGaussLista(6))
print()
print(powtarzajaceZZakresuLista(20, 10))
print(powtarzajaceZZakresuLista(6, 4))
