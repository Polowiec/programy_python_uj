#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

import random


def losowaLista(zakres):
    if zakres < 1:
        raise ValueError
    lista = [x for x in range(zakres)]

    for x in range(zakres):
        i1 = random.randint(0, zakres - 1)
        i2 = random.randint(0, zakres - 1)
        lista[i1], lista[i2] = lista[i2], lista[i1]
    return lista


def prawiePosortowanaLista(zakres):
    if zakres < 1:
        raise ValueError
    lista = [x for x in range(zakres)]
    if zakres <= 1:
        return lista

    lista.sort()
    # delikatne mieszanie
    for x in range(int(zakres / 10) + 1):
        poz = random.randint(0, zakres - 2)
        odleglos = random.randint(1, 2)
        poz2 = min(zakres - 1, poz + odleglos)
        lista[poz], lista[poz2] = lista[poz2], lista[poz]
    return lista


def prawieOdwrotniePosortowanaLista(zakres):
    lista = prawiePosortowanaLista(zakres)
    lista.reverse()
    return lista


def losowyGaussLista(ilosc):
    if ilosc < 0:
        raise ValueError
    lista = [random.gauss(0, 1) for x in range(ilosc)]
    return lista


def powtarzajaceZZakresuLista(ilosc, zakres):
    # warunek zakres < ilosc
    if ilosc < 0 or zakres < 1:
        raise ValueError
    if ilosc <= zakres:
        raise ValueError

    lista = [random.randint(0, zakres - 1) for x in range(ilosc)]
    return lista
