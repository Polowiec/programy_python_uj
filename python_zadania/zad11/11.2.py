#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

from generatelist import *
from selectsort_analyze import *

rozmiar = 10
lista = losowaLista(rozmiar)
selectsort(lista, 0, rozmiar - 1)
print(lista)
