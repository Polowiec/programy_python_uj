#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'

import time
from generatelist import *
from quicksort import *
from selectsort_cmpfunc import *
from shellsort import *
from mergesort import *

for rozmiar in (10 ** 2, 10 ** 3, 10 ** 4, 10 ** 5, 10 ** 6):
    l1 = losowaLista(rozmiar)
    print("Rozmiar: ", rozmiar)

    tmp = list(l1)
    start = time.clock()
    quicksort(tmp, 0, rozmiar - 1)
    stop = time.clock()
    print("quicksort: ", stop - start)

    tmp = list(l1)
    start = time.clock()
    selectsort(tmp, 0, rozmiar - 1)
    stop = time.clock()
    print("selectsort: ", stop - start)

    tmp = list(l1)
    start = time.clock()
    shellsort(tmp, 0, rozmiar - 1)
    stop = time.clock()
    print("shellsort: ", stop - start)

    tmp = list(l1)
    start = time.clock()
    mergesort(tmp, 0, rozmiar - 1)
    stop = time.clock()
    print("mergesort: ", stop - start)
    print()
