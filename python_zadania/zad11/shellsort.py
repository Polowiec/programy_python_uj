#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


def shellsort(L, left, right):
    h = int((right - left) / 2)
    while h > 0:
        for i in range(int(left + h), right + 1):
            for j in range(i, left + h - 1, -h):
                if L[j - h] > L[j]:
                    swap(L, j - h, j)
        h = int(h / 2)


def swap(L, i, k):
    L[i], L[k] = L[k], L[i]
