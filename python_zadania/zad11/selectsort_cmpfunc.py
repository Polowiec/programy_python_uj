#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Tomasz Polowiec'


def default_cmp(a, b):
    if a < b:
        return -1
    elif a == b:
        return 0
    else:
        return 1


def selectsort(L, left, right, cmpfunc=default_cmp):
    for i in range(left, right):
        k = i
        for j in range(i + 1, right + 1):
            if cmpfunc(L[j], L[k]) == -1:
                k = j
        swap(L, i, k)


def swap(L, i, k):
    L[i], L[k] = L[k], L[i]
